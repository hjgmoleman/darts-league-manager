import 'regenerator-runtime/runtime'
import React, { Suspense } from 'react'
import ReactDOM from 'react-dom'
import { BrowserRouter } from 'react-router-dom'
import { 
    MantineProvider, 
    Global, 
    MantineTheme 
} from '@mantine/core'
import { Provider } from 'urql';

import client from './api/client'
import './index.css'
import {Shell} from './app/Shell'
import colors from './shared/colors'
import reportWebVitals from './shared/monitoring/reportWebVitals'

const fontFamily = "'Nunito Sans', sans-serif"

const themeOverrides = {
    colors,
    fontFamily,
    headings: {
        fontFamily
    }
}

const globalStyles = (theme: MantineTheme) => ({
    body: {
        ...theme.fn.fontStyles(),
        color: theme.colors.slate[7],
        '--text-color': theme.colors.slate[7],
        '--border-color': theme.colors.slate[2],
        '--border-width': '1px',
        '--border-style': 'solid'
    } 
})

ReactDOM.render(
    <React.StrictMode>
        <Provider value={client}>
            <MantineProvider 
                withNormalizeCSS 
                withCSSVariables
                withGlobalStyles
                theme={themeOverrides}
            >
                <Global styles={globalStyles} />
                <BrowserRouter>
                
                    <Suspense fallback={'Loading...'}></Suspense>
                    <Shell />
                </BrowserRouter>
            </MantineProvider>
        </Provider>
    </React.StrictMode>,
    document.getElementById('root')
)

reportWebVitals(console.log)