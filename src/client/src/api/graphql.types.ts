export type Maybe<T> = T | null;
export type InputMaybe<T> = Maybe<T>;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]?: Maybe<T[SubKey]> };
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]: Maybe<T[SubKey]> };
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
};

export type Mutation = {
  __typename?: 'Mutation';
  updateTournament: UpdateTournamentOutput;
};


export type MutationUpdateTournamentArgs = {
  data: UpdateTournamentInput;
};

/** The node interface is implemented by entities that have a global unique identifier. */
export type Node = {
  id: Scalars['ID'];
};

export type Player = {
  __typename?: 'Player';
  id: Scalars['ID'];
};

export type Query = {
  __typename?: 'Query';
  /** Fetches an object given its ID. */
  node?: Maybe<Node>;
  /** Lookup nodes by a list of IDs. */
  nodes: Array<Maybe<Node>>;
  player: Player;
  players: Array<Player>;
  tournament?: Maybe<Tournament>;
  tournaments: Array<Tournament>;
};


export type QueryNodeArgs = {
  id: Scalars['ID'];
};


export type QueryNodesArgs = {
  ids: Array<Scalars['ID']>;
};


export type QueryPlayerArgs = {
  playerId: Scalars['ID'];
};


export type QueryTournamentArgs = {
  id: Scalars['ID'];
};

export type Tournament = Node & {
  __typename?: 'Tournament';
  id: Scalars['ID'];
  rounds: Array<TournamentRound>;
  title: Scalars['String'];
};

export type TournamentRound = {
  __typename?: 'TournamentRound';
  id: Scalars['ID'];
  numberOfGroups: Scalars['Int'];
  playerCount: Scalars['Int'];
  roundType: Scalars['Int'];
  sequenceNumber: Scalars['Int'];
};

export type UpdateTournamentInput = {
  id: Scalars['ID'];
  title: Scalars['String'];
};

export type UpdateTournamentOutput = {
  __typename?: 'UpdateTournamentOutput';
  message?: Maybe<Scalars['String']>;
  success: Scalars['Boolean'];
};
