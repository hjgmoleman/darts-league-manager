import { createClient, dedupExchange, fetchExchange } from 'urql'
import { suspenseExchange } from "@urql/exchange-suspense";
import { cacheExchange } from '@urql/exchange-graphcache'
import { devtoolsExchange } from '@urql/devtools'

import schema from './graphql.schema'
const GraphEndpoint: string = import.meta.env.VITE_GRAPH_ENDPOINT as string

export default createClient({
    url: GraphEndpoint,
    suspense: true,
    exchanges: [devtoolsExchange, dedupExchange, suspenseExchange, cacheExchange({
        schema
    }), fetchExchange]
})