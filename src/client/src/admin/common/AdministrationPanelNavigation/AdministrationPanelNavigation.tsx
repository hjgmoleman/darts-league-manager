import { SideNavigation } from '@darts/shared/SideNavigation'

export default function() {
    return (
        <SideNavigation title="Admin">
            <SideNavigation.Item 
                text='Tournaments'
                to='tournaments'
            />

            <SideNavigation.Item 
                text='Players'
                to='players'
            />
        </SideNavigation>
    )
}
