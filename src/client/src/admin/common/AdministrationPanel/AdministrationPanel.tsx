import { Suspense } from 'react'
import { Route, Routes } from 'react-router-dom'
import { VerticalNavLayout } from '@darts/shared/layouts/VerticalNavLayout'

import { AdministrationPanelNavigation } from '../AdministrationPanelNavigation'
import TournamentRoutes from '../../tournaments/Routes'

const LoadingIndicator = () => <div>loading...</div>

export function AdministrationPanel() {
    return (
        <VerticalNavLayout
            aside={
                <AdministrationPanelNavigation />
            }
        >
            <Suspense fallback={<LoadingIndicator />}>
                <Routes>
                    <Route path='tournaments/*'
                        element={<TournamentRoutes />}
                    />
                </Routes>
            </Suspense>
        </VerticalNavLayout>
    )
}