import { useParams } from 'react-router-dom'
import { TournamentDetails } from '../TournamentDetails'

export default function () {
    const { id } = useParams()
    return (
        <TournamentDetails id={id} />
    )
}