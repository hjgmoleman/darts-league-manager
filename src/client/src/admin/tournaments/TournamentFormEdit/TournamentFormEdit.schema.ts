import { z } from 'zod'

const schema = z.object({
    title: z.string()
})

export default schema
export type TournamentFormEditData = z.infer<typeof schema>