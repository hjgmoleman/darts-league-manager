import type { TournamentFormEditData } from './TournamentFormEdit.schema'

import { useForm, zodResolver } from '@mantine/form'
import { Button, TextInput, Group } from '@mantine/core'
import schema from './TournamentFormEdit.schema'

interface TournamentFormEditProps {
    initialValues?: TournamentFormEditData,
    onSubmit: (value: TournamentFormEditData) => void
}

export default function(props: TournamentFormEditProps) {
    const { 
        initialValues, 
        onSubmit 
    } = props

    const form = useForm<TournamentFormEditData>({
        initialValues,
        schema: zodResolver(schema)
    })
    
    return (
        <form onSubmit={form.onSubmit(onSubmit)}>
            <TextInput 
                label="Title" 
                {...form.getInputProps('title')}
            />
            
            <Group mt="md">
                <Button type="submit">Update</Button>
            </Group>
        </form>
    )
}