import { List } from '@mantine/core'
import { SuspenseBoundary } from '@darts/shared/SuspenseBoundary'

import { TournamentListComponent } from './TournamentList.generated'
import TournamentListItem from './ListItem/ListItem'

export default function() {
    return (
        <List>
            <SuspenseBoundary>
                <TournamentListComponent>
                    { ({ data }, ) =>(
                        <>
                            { data.tournaments.map(tournament => (
                                <TournamentListItem 
                                    key={tournament.id} 
                                    data={tournament}
                                />
                            )) }
                        </>
                    )}
                </TournamentListComponent>
            </SuspenseBoundary>
        </List>
    )
}