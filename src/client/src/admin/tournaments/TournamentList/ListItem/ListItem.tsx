import { TournamentListItem_DataFragment } from './ListItem.generated'
import { Link } from 'react-router-dom'
import { List } from '@mantine/core'

interface Props {
    data: TournamentListItem_DataFragment
}

export default function(props: Props) {
    const {
        id,
        title
    } = props.data

    return (
        <List.Item>
            <Link to={`/admin/tournaments/${id}`}>
                { title }
            </Link>
        </List.Item>
    )
}