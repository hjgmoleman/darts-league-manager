import * as Types from '@darts/api/graphql.types';

import { gql } from 'urql';
export type TournamentListItem_DataFragment = { __typename?: 'Tournament', id: string, title: string };

export const TournamentListItem_DataFragmentDoc = gql`
    fragment TournamentListItem_data on Tournament {
  id
  title
}
    `;