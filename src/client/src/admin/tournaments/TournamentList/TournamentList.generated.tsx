import * as Types from '@darts/api/graphql.types';

import { gql } from 'urql';
import { TournamentListItem_DataFragmentDoc } from './ListItem/ListItem.generated';
import * as React from 'react';
import * as Urql from 'urql';
export type Omit<T, K extends keyof T> = Pick<T, Exclude<keyof T, K>>;
export type TournamentListQueryVariables = Types.Exact<{ [key: string]: never; }>;


export type TournamentListQuery = { __typename?: 'Query', tournaments: Array<{ __typename?: 'Tournament', id: string, title: string }> };


export const TournamentListDocument = gql`
    query TournamentList {
  tournaments {
    id
    ...TournamentListItem_data
  }
}
    ${TournamentListItem_DataFragmentDoc}`;

export const TournamentListComponent = (props: Omit<Urql.QueryProps<TournamentListQuery, TournamentListQueryVariables>, 'query'> & { variables?: TournamentListQueryVariables }) => (
  <Urql.Query {...props} query={TournamentListDocument} />
);


export function useTournamentListQuery(options?: Omit<Urql.UseQueryArgs<TournamentListQueryVariables>, 'query'>) {
  return Urql.useQuery<TournamentListQuery>({ query: TournamentListDocument, ...options });
};