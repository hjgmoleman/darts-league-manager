import { Heading } from '@darts/shared/Heading'
import { TournamentList } from '../TournamentList'

export default function() {
    return (
        <>
            <Heading text="Tournaments" />
            <TournamentList />
        </>
    )
}