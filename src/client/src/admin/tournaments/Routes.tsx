import { Routes, Route } from 'react-router-dom'

import { TournamentListPage } from './TournamentListPage'
import { TournamentDetailsPage } from './TournamentDetailsPage'

export default function() {
    return (
        <Routes>
            <Route index element={<TournamentListPage />} />
            <Route path=":id" element={<TournamentDetailsPage />} />
        </Routes>
    )
}