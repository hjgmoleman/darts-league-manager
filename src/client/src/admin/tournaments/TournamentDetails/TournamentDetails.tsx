import { TournamentFormEdit } from '../TournamentFormEdit'
import { useGetTournamentQuery, useUpdateTournamentMutation } from './TournamentDetails.generated'
import TournamentRounds from './Rounds/Rounds'
import TournamentHeading from './Heading/Heading'

interface TournamentDetailsProps {
    id: string
}

export default function(props: TournamentDetailsProps) {
    const { 
        id
    } = props

    const [result] = useGetTournamentQuery({ 
        variables: { 
            id 
        }
    })
    
    const [updateTournamentResult, updateTournament] = useUpdateTournamentMutation()
    const handleSubmit = async values => {
        await updateTournament({
            data: {
                ...values,
                id
            }
        })
    }

    const { data } = result

    const initialValues = {
        title: data.tournament.title
    }
    
    return (
        <>
            <TournamentHeading data={data.tournament} />
            <TournamentFormEdit 
                initialValues={initialValues} 
                onSubmit={handleSubmit}
            />
            
            <TournamentRounds 
                data={data.tournament}
            />

            {/* { rounds.map(round => (
                <div>{round.playerCount}</div>
            ))} */}

            {/*
                Tabs for each individual round
            */}
        </>
    )
}