import * as Types from '@darts/api/graphql.types';

import { gql } from 'urql';
import { TournamentRounds_DataFragmentDoc } from './Rounds/Rounds.generated';
import * as React from 'react';
import * as Urql from 'urql';
export type Omit<T, K extends keyof T> = Pick<T, Exclude<keyof T, K>>;
export type GetTournamentQueryVariables = Types.Exact<{
  id: Types.Scalars['ID'];
}>;


export type GetTournamentQuery = { __typename?: 'Query', tournament?: { __typename?: 'Tournament', title: string, rounds: Array<{ __typename?: 'TournamentRound', id: string, playerCount: number, roundType: number }> } | null };

export type UpdateTournamentMutationVariables = Types.Exact<{
  data: Types.UpdateTournamentInput;
}>;


export type UpdateTournamentMutation = { __typename?: 'Mutation', updateTournament: { __typename?: 'UpdateTournamentOutput', success: boolean } };


export const GetTournamentDocument = gql`
    query getTournament($id: ID!) {
  tournament: tournament(id: $id) {
    title
    ...TournamentRounds_data
  }
}
    ${TournamentRounds_DataFragmentDoc}`;

export const GetTournamentComponent = (props: Omit<Urql.QueryProps<GetTournamentQuery, GetTournamentQueryVariables>, 'query'> & { variables: GetTournamentQueryVariables }) => (
  <Urql.Query {...props} query={GetTournamentDocument} />
);


export function useGetTournamentQuery(options: Omit<Urql.UseQueryArgs<GetTournamentQueryVariables>, 'query'>) {
  return Urql.useQuery<GetTournamentQuery>({ query: GetTournamentDocument, ...options });
};
export const UpdateTournamentDocument = gql`
    mutation updateTournament($data: UpdateTournamentInput!) {
  updateTournament(data: $data) {
    success
  }
}
    `;

export const UpdateTournamentComponent = (props: Omit<Urql.MutationProps<UpdateTournamentMutation, UpdateTournamentMutationVariables>, 'query'> & { variables?: UpdateTournamentMutationVariables }) => (
  <Urql.Mutation {...props} query={UpdateTournamentDocument} />
);


export function useUpdateTournamentMutation() {
  return Urql.useMutation<UpdateTournamentMutation, UpdateTournamentMutationVariables>(UpdateTournamentDocument);
};