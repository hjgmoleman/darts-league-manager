import { Tabs } from '@mantine/core'
import { TournamentRounds_DataFragment } from './Rounds.generated'

interface TournamentDetailsRoundsProps {
    data: TournamentRounds_DataFragment
}

export default function(props: TournamentDetailsRoundsProps) {
    const {
        rounds
    } = props.data
    
    if (rounds.length === 0) {
        return <div />
    }

    return (
        <Tabs>
            {rounds.map(round => (
                <Tabs.Tab 
                    key={round.id}
                    label={round.roundType}> 
                    {round.__typename}
                </Tabs.Tab>
            ))}            
        </Tabs>
    )
}