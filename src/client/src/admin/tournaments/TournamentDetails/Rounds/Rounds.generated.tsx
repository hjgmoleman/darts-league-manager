import * as Types from '@darts/api/graphql.types';

import { gql } from 'urql';
export type TournamentRounds_DataFragment = { __typename?: 'Tournament', rounds: Array<{ __typename?: 'TournamentRound', id: string, playerCount: number, roundType: number }> };

export const TournamentRounds_DataFragmentDoc = gql`
    fragment TournamentRounds_data on Tournament {
  rounds {
    id
    playerCount
    roundType
  }
}
    `;