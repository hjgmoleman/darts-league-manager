import type { TournamentDetailsHeading_DataFragment } from './Heading.generated'
import { Heading } from '@darts/shared/Heading'

interface TournamentDetailsHeadingProps {
    data: TournamentDetailsHeading_DataFragment
}

export default function(props: TournamentDetailsHeadingProps) {
    const {
        title
    } = props.data

    return <Heading text={title} />
}