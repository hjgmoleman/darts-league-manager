import * as Types from '@darts/api/graphql.types';

import { gql } from 'urql';
export type TournamentDetailsHeading_DataFragment = { __typename?: 'Tournament', title: string };

export const TournamentDetailsHeading_DataFragmentDoc = gql`
    fragment TournamentDetailsHeading_data on Tournament {
  title
}
    `;