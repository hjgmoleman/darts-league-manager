import { Routes, Route } from 'react-router-dom'
import { VerticalNavLayout } from '@darts/shared/layouts/VerticalNavLayout'

import { TournamentNavigation } from '../TournamentNavigation'

import useStyles from './Dashboard.styles'

export function Dashboard() {
    const { classes } = useStyles()

    return (
        <VerticalNavLayout
            aside={
                <TournamentNavigation />
            }
        >
            <Routes>
                <Route 
                    path='/:id'
                    element={<div>12345</div>}
                />
            </Routes>
        </VerticalNavLayout>
    )
}