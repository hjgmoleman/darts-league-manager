import { Link } from 'react-router-dom'

export default function() {
    return (
        <nav>
            <Link to='rounds/1'>Round 1</Link>
            <Link to='rounds/2'>Round 2</Link>
        </nav>
    )
}