import { border } from 'polished'
import { createStyles } from '@mantine/core'

export default createStyles(theme => ({
    root: {
        display: 'flex',
        flexDirection: 'column',
        height: '100%',
        ...border('right', 'var(--border-width)', 'var(--border-style)', 'var(--border-color)')
        //backgroundColor: theme.colors.zinc[6]
    },

    brand: {
        height: '4rem'
    },

    navigation: {
        flex: 1,
        alignItems: 'flex-start',
        display: 'flex'
    },

    widgets: {
        height: 'auto',
        width: '100%',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'flex-end',
    }
}))