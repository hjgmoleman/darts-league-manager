import { MainNavigation } from '../MainNavigation'
import useStyles from './ShellNavigationbar.styles'

export default function() {
    const { classes } = useStyles()

    return (
        <nav className={classes.root}>
            <div className={classes.brand} />
            <div className={classes.navigation}>
                <MainNavigation />
            </div>
            
            <div className={classes.widgets}>
                
            </div>
        </nav>
    )
}