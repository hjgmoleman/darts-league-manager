import { border } from 'polished'
import { createStyles } from '@mantine/core'

export const useStyles = createStyles(theme => ({
    base: {
        position: 'relative',
        height: '4rem',
        width: '100%',
        color: theme.colors.indigo[7],
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center'
    },

    active: {
        // color: theme.colors.indigo[7],
        // fontWeight: 700,
        // ...border('bottom', 1, theme.colors.indigo[7], 'solid')
    },
}))