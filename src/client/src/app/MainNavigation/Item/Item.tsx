import type { PropsWithChildren } from 'react'

import { Center } from '@mantine/core'
import { NavLink } from 'react-router-dom'
import { useStyles } from './Item.styles'

export type MainNavigationItemProps = PropsWithChildren<{
    className?: string
    path: string
}>

export default function(props: MainNavigationItemProps) {
    const {
        path,
        className,
        children
    } = props

    const { classes, cx } = useStyles()

    return (
        <li className={cx(classes.base, className)}>
            <NavLink
                to={path} 
                className={({ isActive }) => cx({
                    [classes.active]: isActive
                })}
            >
                <Center inline>
                    {children}
                </Center>
            </NavLink>
        </li>
    )
}