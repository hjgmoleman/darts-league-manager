import type { PropsWithChildren } from 'react'
import { Dashboard, Adjustments } from 'tabler-icons-react'

import useStyles from './MainNavigation.styles'
import Item from './Item/Item'

export type MainNavigationProps = PropsWithChildren<{}>

export function MainNavigation(props: MainNavigationProps) {

    const { classes } = useStyles(null, {
        name: 'MainNavigation'
    })

    return (
        <div className={classes.root}>
            <ul className={classes.list}>
                <Item path="/dashboard"><Dashboard /></Item>
                <Item path="/admin"><Adjustments /></Item>
            </ul>
        </div>
    )
}
