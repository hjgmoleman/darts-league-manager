import { createStyles } from '@mantine/core'

export default createStyles(theme => ({
    root: {
        height: '100%',
        width: '100%',
        display: 'flex'
    },

    list: {
        listStyle: 'none',
        padding: 0,
        margin: 0,
        width: '100%'
    },

    icon: {
        color: theme.colors.gray[7]
    }
}))
