import { Routes, Route, Navigate } from 'react-router-dom'
import { Dashboard } from '@darts/dashboard/Dashboard'
import { AdministrationPanel } from '@darts/admin/common/AdministrationPanel'

import { ShellNavigationbar } from '../ShellNavigationbar'
import useStyles from './Shell.styles'

export default function() {

    const { classes } = useStyles()

    return (
        <div className={classes.root}>
            <div className={classes.aside}>
                <ShellNavigationbar />
            </div>
        
            <main className={classes.main}>
                <Routes>
                    <Route path="/" element={
                        <Navigate to="/dashboard" replace={true} />
                    }/>
                    <Route path="/dashboard/*" element={<Dashboard />} />
                    <Route path="admin/*" element={<AdministrationPanel />} />
                </Routes>
            </main>
        </div>
    )
}