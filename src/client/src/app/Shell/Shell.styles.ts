import { createStyles } from '@mantine/core'

export default createStyles(theme => ({
    root: {
        height: '100vh',
        width: '100vw',
        position: 'absolute',
        //backgroundColor: theme.colors.gray[1],
        display: 'flex',
        overflow: 'hidden'
    },

    aside: {
        position: 'relative',
        width: '4rem',
        zIndex: 1
    },

    main: {
        position: 'relative',
        //boxShadow: '-5px 0px 10px 4px rgba(0,0,0,0.25)',
        height: '100%',
        width: '100%',
        zIndex: 2,
    }
}))