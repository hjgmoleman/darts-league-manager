import type { PropsWithChildren } from 'react'
import { Heading } from '../Heading'
import SideNavigationItem from './SideNavigationItem'
import useStyles from './SideNavigation.styles'

export type SideNavigationProps = PropsWithChildren<{
    className?: string
    title?: string
}>

function SideNavigation(props: SideNavigationProps) {

    const {
        className,
        title,
        children
    } = props

    const { classes, cx } = useStyles()
 
    return (
        <nav className={cx(classes.root, className)}>
            { title && (
                <Heading 
                    text={title} 
                    className={classes.heading}
                />
            )}

            <ul className={classes.list}>
                {children}
            </ul>
        </nav>
    )
}

SideNavigation.Item = SideNavigationItem

export default SideNavigation