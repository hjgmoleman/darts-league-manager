import { border } from 'polished'
import { createStyles } from '@mantine/core'

export default createStyles(theme => ({
    root: {
        padding: 0,
        margin: 0
    },
    heading: {
        textTransform: 'uppercase'
    },
    list: {
        listStyle: 'none',
        padding: 0,
        margin: 0,
        ...border('top', 'var(--border-width)', 'var(--border-style)', 'var(--border-color)')
    }
}))