import { 
    useResolvedPath, 
    useMatch,
    Link
} from 'react-router-dom'
import { Text } from '@mantine/core'
import useStyles from './SideNavigationItem.styles'

export interface SideNavigationItemProps {
    text?: string
    description?: string
    to: string
}

export default function(props: SideNavigationItemProps) {
    const {
        text,
        description,
        to
    } = props

    let resolved = useResolvedPath(to)
    let match = useMatch({ 
        path: resolved.pathname, 
        end: true 
    })

    const { classes, cx } = useStyles()

    return (
        <li className={cx(classes.root, {
            [classes.active]: match
        })}>
            <Link to={to}
                className={classes.link}
            >
                <Text className={classes.text} size='sm'>
                    {text}
                </Text>

                { description && (
                    <div className={classes.description}>
                        {description}
                    </div>
                )}
            </Link>
        </li>
    )
}