import { 
    border,
    padding 
} from 'polished'
import { createStyles } from '@mantine/core'

export default createStyles(theme => ({
    root: {
        '--color': theme.colors.blue[7],
        ...padding(theme.spacing.sm, theme.spacing.md, theme.spacing.sm, 40),
        
        ':hover': {
            //backgroundColor: theme.white,
        }
    },

    link: {

    },

    text: {
        fontWeight: 'normal',
        textTransform: 'uppercase'
    },

    description: {

    },

    active: {
        backgroundColor: theme.colors.slate[1]
    }
}))