import { Title } from '@mantine/core'
import useStyles from './Heading.styles'

export interface HeadingProps {
    text: string,
    className?: string
}

export default function(props: HeadingProps) {
    const {
        text,
        className
    } = props

    const { classes, cx } = useStyles()

    return (
        <Title 
            order={2}
            className={cx(classes.title, className)}
        >
            {text}
        </Title>
    )
}