import { createStyles } from '@mantine/core'

export default createStyles(theme => ({
    title: {
        height: '4rem',
        padding: theme.spacing.md,
        fontWeight: 'normal'
    },
}))