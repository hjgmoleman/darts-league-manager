import type { PropsWithChildren, ReactElement } from 'react'
import { useStyles } from './VerticalNavLayout.styles'

interface Props {
    aside?: ReactElement
    reverse?: boolean
}

export type VerticalNavLayout = PropsWithChildren<Props>

export default function(props: VerticalNavLayout) {
    const {
        children,
        aside,
        reverse = false
    } = props

    const { classes } = useStyles({ reverse })

    return (
        <div className={classes.root}>
            <aside className={classes.aside}>
                {aside}
            </aside>
            <section className={classes.section}>
                {children}
            </section>
        </div>
    )
}