import { border} from 'polished'
import { createStyles } from '@mantine/core'

interface VerticalNavLayoutStyleParams {
    reverse?: boolean
}

export const useStyles = createStyles((
    theme, params: VerticalNavLayoutStyleParams) => ({
    root: {
        width: '100%',
        height: '100%',
        flex: 1,
        display: 'flex',
        flexDirection: params.reverse ? 'row-reverse' : 'row',
        //backgroundColor: theme.fn.lighten(theme.colors.gray[1], 0.5)
    },

    aside: {
        display: 'flex',
        flexDirection: 'column',
        flexShrink: 0,  
        width: '16rem',
        //backgroundColor: theme.white
    },

    section: {
        flex: 1,
        ...border(params.reverse ? 'right' : 'left', 'var(--border-width)', 'var(--border-style)', 'var(--border-color)')
    }
}))