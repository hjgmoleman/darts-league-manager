import { padding } from 'polished'
import { createStyles } from '@mantine/core'
import { LayoutConstants } from '../constants'

export default createStyles(theme => ({
    root: {
        display: 'flex',
        flexDirection: 'column'
    },
    
    header: {
        flexShrink: 0,
        height: LayoutConstants.AppHeaderHeight,
        borderBottom: `1px solid ${theme.colors.gray[4]}`,
        display: 'flex',
        alignItems: 'center',
    },

    body: {
        flex: '1 1 auto',
    },
    
    footer: {
        flexShrink: 0,
        height: LayoutConstants.AppFooterHeight,
    }
}))
