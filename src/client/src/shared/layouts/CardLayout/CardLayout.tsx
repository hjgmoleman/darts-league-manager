import type { ReactElement, PropsWithChildren } from 'react'
import type { ClassNames, DefaultProps } from '@mantine/core'

import { Center } from '@mantine/core'
import useStyles from './CardLayout.styles'

export type CardLayoutStylesNames = ClassNames<typeof useStyles>

export interface CardLayoutProps extends PropsWithChildren<DefaultProps<CardLayoutStylesNames>> {
    hideFooter?: boolean
    hideHeader?: boolean
    header?: ReactElement
    footer?: ReactElement
}

export default function CardLayout(props: CardLayoutProps) {
    const {
        header,
        footer,
        children,
        styles,
        classNames,
        className,
        hideHeader,
        hideFooter
    } = props

    const { classes, cx } = useStyles(null, {
        classNames,
        styles,
        name: 'CardLayout'
    })

    return (
        <div className={cx(classes.root, className)}>
            { !hideHeader && (
                <header className={classes.header}>
                    { header }
                </header>
            ) }
            
            <div className={classes.body}>
                {children}
            </div>
            
            { !hideFooter && (
                <footer className={classes.footer}>
                    { footer }
                </footer>
            ) }
        </div>
    )
}