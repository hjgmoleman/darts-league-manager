import { Suspense } from 'react'
import { Title } from '@mantine/core'

export default function({ children }) {
    const fallback = <Title order={1}>Suspense Loading!</Title>
    return (
        <Suspense fallback={fallback}>{children}</Suspense>
    )
}