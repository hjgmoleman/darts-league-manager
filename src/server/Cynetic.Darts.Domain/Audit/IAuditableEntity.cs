using System;
using Cynetic.Darts.Domain.Common;

namespace Cynetic.Darts.Domain.Audit;

public interface IAuditableEntity<out TId> : IAuditableEntity, IEntity<TId>
{
}
    
public interface IAuditableEntity : IEntity
{
    public string CreatedBy { get; set; }
    public DateTime CreatedOn { get; set; }
    public string LastModifiedBy { get; set; }
    public DateTime? LastModifiedOn { get; set; }
}