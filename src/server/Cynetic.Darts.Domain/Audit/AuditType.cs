using Cynetic.Darts.Domain.Common;

namespace Cynetic.Darts.Domain.Audit;

public class AuditType : Enumeration
{
    public static readonly AuditType None = new(0, nameof(None));
    public static readonly AuditType Create = new(1, nameof(Create));
    public static readonly AuditType Update = new(1, nameof(Update));
    public static readonly AuditType Delete = new(1, nameof(Delete));
        
    private AuditType(int value, string displayName)
        : base(value, displayName)
    {
    }
}