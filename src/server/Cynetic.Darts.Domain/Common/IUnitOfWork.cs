using System.Threading;
using System.Threading.Tasks;

namespace Cynetic.Darts.Domain.Common;

public interface IUnitOfWork
{
    Task<int> Commit(CancellationToken cancellationToken);
    void Rollback();
}