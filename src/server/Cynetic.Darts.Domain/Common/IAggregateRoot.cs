using System.Collections.Generic;

namespace Cynetic.Darts.Domain.Common;

public interface IAggregateRoot
{
    IEnumerable<object> GetDomainEvents();
    void ClearDomainEvents();
}