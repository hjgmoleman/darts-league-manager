using System;

namespace Cynetic.Darts.Domain.Common;

public interface IConfigureInstanceEventRouter : IInstanceEventRouter
{
    void ConfigureRoute(Type aggregateEventType, Action<object>? handler);
    void ConfigureRoute<TEvent>(Action<TEvent> handler);
}