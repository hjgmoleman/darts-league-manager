using System.Threading;
using System.Threading.Tasks;

namespace Cynetic.Darts.Domain.Common;

public interface IRepository<TAggregateRoot, in TId> 
    where TAggregateRoot : IAggregateRoot, IEntity<TId>
{
    Task<TAggregateRoot> GetAsync(TId id, CancellationToken cancellationToken = default);
}