using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Cynetic.Darts.Domain.Common;

public abstract class Enumeration : IComparable
{
    public string Name { get; }

    public int Id { get; }

    protected Enumeration(int id, string name)
    {
        Id = id;
        Name = name;
    }

    public override string ToString() => Name;

    public static IEnumerable<T> GetAll<T>() 
        where T : Enumeration
    {
        var fields = typeof(T).GetFields(BindingFlags.Public | BindingFlags.Static | BindingFlags.DeclaredOnly);

        return fields.Select(f => f.GetValue(null)).Cast<T>();
    }

    public override bool Equals(object? obj)
    {
        if (!(obj is Enumeration otherValue))
        {
            return false;
        }

        var typeMatches = GetType() == obj.GetType();
        var valueMatches = Id.Equals(otherValue.Id);

        return typeMatches && valueMatches;
    }

    public override int GetHashCode() => Id.GetHashCode();

    public static int AbsoluteDifference(Enumeration firstValue, Enumeration secondValue)
    {
        var absoluteDifference = Math.Abs(firstValue.Id - secondValue.Id);
        return absoluteDifference;
    }

    public static T FromValue<T>(int value) where T : Enumeration
    {
        var matchingItem = Parse<T, int>(value, "value", item => item.Id == value);
        return matchingItem;
    }

    public static T FromDisplayName<T>(string displayName) where T : Enumeration
    {
        var matchingItem = Parse<T, string>(displayName, "display name", item => item.Name == displayName);
        return matchingItem;
    }

    public static T FromDisplayName<T>(string displayName, StringComparison comparison) where T : Enumeration
    {
        var matchingItem = Parse<T, string>(displayName, "display name", item => item.Name.Equals(displayName, comparison));
        return matchingItem;
    }
        
    private static T Parse<T, K>(K value, string description, Func<T, bool> predicate) where T : Enumeration
    {
        var matchingItem = GetAll<T>().FirstOrDefault(predicate);

        if (matchingItem == null)
        {
            throw new InvalidOperationException($"'{value}' is not a valid {description} in {typeof(T)}");
        }

        return matchingItem;
    }

    public int CompareTo(object? obj) => Id.CompareTo((obj as Enumeration)?.Id);

    public static bool operator == (Enumeration? left, Enumeration? right)
    {
        if (ReferenceEquals(left, null))
        {
            return ReferenceEquals(right, null);
        }
            
        return right is not null && left.Equals(right);
    }

    public static bool operator > (Enumeration left, Enumeration right)
    {
        return left.CompareTo(right) > 0;
    }
        
    public static bool operator < (Enumeration left, Enumeration right)
    {
        return left.CompareTo(right) < 0;
    }
        
    public static bool operator != (Enumeration? left, Enumeration? right)
    {
        return !(left == right);
    }
        
    public static bool operator <= (Enumeration left, Enumeration right)
    {
        return left == right || left < right;
    }

    public static bool operator >=(Enumeration left, Enumeration right)
    {
        return left == right || left > right;
    }
        
    public static implicit operator string(Enumeration enumeration) => enumeration.Name;
    public static implicit operator int(Enumeration enumeration) => enumeration.Id;
}