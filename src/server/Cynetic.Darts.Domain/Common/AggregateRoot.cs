using System;
using System.Collections.Generic;
using System.Linq;

namespace Cynetic.Darts.Domain.Common;

public class AggregateRoot<TId> : Entity<TId>, IAggregateRoot
{
    private readonly EventRecorder _recorder = new EventRecorder();
    private readonly InstanceEventRouter _router = new InstanceEventRouter();

    protected AggregateRoot()
    {
    }
        
    protected AggregateRoot(TId id)
        :base(id)
    {
    }

    public IEnumerable<object> GetDomainEvents()
    {
        return _recorder.ToList();
    }

    public void ClearDomainEvents()
    {
        _recorder.Reset();
    }

    protected void Emit<T>(T domainEvent)
    {
        if (domainEvent is null)
        {
            throw new ArgumentNullException(nameof(domainEvent));
        }
            
        _recorder.Record(domainEvent);
    }
        
    protected void Register<TEvent>(Action<TEvent> handler)
    {
        _router.ConfigureRoute(handler);
    }
        
    protected void Register(Type eventType, Action<object>? handler)
    {
        _router.ConfigureRoute(eventType, handler);
    }
        
    protected virtual void Apply(object domainEvent)
    {
        _router.Route(domainEvent);
    }
        
    protected void Emit(object domainEvent)
    {
        Apply(domainEvent);
        Record(domainEvent);
    }

    private void Record(object domainEvent)
    {
        _recorder.Record(domainEvent);
    }
}