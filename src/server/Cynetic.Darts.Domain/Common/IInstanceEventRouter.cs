namespace Cynetic.Darts.Domain.Common;

public interface IInstanceEventRouter
{
    void Route(object aggregateEvent);
}