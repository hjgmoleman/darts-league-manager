using System.Diagnostics.CodeAnalysis;

namespace Cynetic.Darts.Domain.Common;

[SuppressMessage("Microsoft.Design", "CA1040:AvoidEmptyInterfaces")]
public interface IEntity
{
}
    
public interface IEntity<out TId> : IEntity
{
    public TId Id { get; }
}