using System.Collections;
using System.Collections.Generic;

namespace Cynetic.Darts.Domain.Common
{
    public class EventRecorder : IEnumerable<object>
    {
        private readonly List<object> events = new ();
        
        public void Record(object aggregateEvent)
        {
            this.events.Add(aggregateEvent);
        }

        public void Reset()
        {
            this.events.Clear();
        }
        
        public IEnumerator<object> GetEnumerator()
        {
            return this.events.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}