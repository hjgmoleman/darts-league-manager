using System;
using System.Collections.Generic;

namespace Cynetic.Darts.Domain.Common;

public class InstanceEventRouter : IConfigureInstanceEventRouter
{
    private readonly Dictionary<Type, Action<object>?> handlers;

    public InstanceEventRouter()
    {
        this.handlers = new Dictionary<Type, Action<object>?>();
    }

    public void ConfigureRoute(Type aggregateEventType, Action<object>? handler)
    {
        if (aggregateEventType == null)
        {
            throw new ArgumentNullException(nameof(aggregateEventType));
        }

        if (handler == null)
        {
            throw new ArgumentNullException(nameof(handler));
        }
            
        this.handlers.Add(aggregateEventType, handler);
    }

    public void ConfigureRoute<TEvent>(Action<TEvent> handler)
    {
        if (handler == null)
        {
            throw new ArgumentNullException(nameof(handler));
        }
            
        this.handlers.Add(typeof (TEvent), @event => handler((TEvent) @event));
    }

    public void Route(object aggregateEvent)
    {
        if (aggregateEvent == null)
        {
            throw new ArgumentNullException(nameof(aggregateEvent));
        }

        if (this.handlers.TryGetValue(aggregateEvent.GetType(), out var handler))
        {
            handler?.Invoke(aggregateEvent);
        }
    }
}