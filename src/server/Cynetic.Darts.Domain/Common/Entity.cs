using System.Diagnostics.CodeAnalysis;

namespace Cynetic.Darts.Domain.Common;

public class Entity<TId> : IEntity<TId>
{
    protected Entity()
    {
    }
        
    protected Entity(TId id)
        : this()
    {
        Id = id;
    }

    public TId Id { get; } = default!;
        
    public override bool Equals(object? obj)
    {
        if (ReferenceEquals(obj, null))
        {
            return false;
        }
                
        if (!(obj is Entity<TId> other))
        {
            return false;
        }

        if (ReferenceEquals(this, other))
        {
            return true;
        }

        if (GetType() != other.GetType())
        {
            return false;
        }

        if (Id!.Equals(default(TId)) || other.Id!.Equals(default(TId)))
        {
            return false;
        }

        return Id.Equals(other.Id);
    }

    public static bool operator ==(Entity<TId>? a, Entity<TId>? b)
    {
        if (a is null && b is null)
        {
            return true;
        }

        if (a is null || b is null)
        {
            return false;
        }

        return a.Equals(b);
    }

    public static bool operator !=(Entity<TId> a, Entity<TId> b)
    {
        return !(a == b);
    }

    public override int GetHashCode()
    {
        return (GetType().ToString() + Id).GetHashCode();
    }
}