using System;
using Cynetic.Darts.Domain.Common;
using Cynetic.Darts.Domain.Players.Events;

namespace Cynetic.Darts.Domain.Players
{
    public class Player : AggregateRoot<PlayerId>
    {
        public Player(PlayerName name)
            : this(PlayerId.New, name)
        {
            Emit(new PlayerRegistered(name));
        }
        
        public Player(PlayerId id, PlayerName name)
            : base(id)
        {
            Name = name ?? throw new ArgumentNullException(nameof(name));
        }

        public PlayerName? Name { get; }
    }
}