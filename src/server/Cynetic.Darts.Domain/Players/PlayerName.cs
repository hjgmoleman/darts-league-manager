using System;

namespace Cynetic.Darts.Domain.Players;

public record PlayerName
{
    public PlayerName(string name)
    {
        if (string.IsNullOrWhiteSpace(name))
        {
            throw new ArgumentException("A player name cannot be empty.", nameof(name));
        }

        Name = name;
    }
    
    public string Name { get; }
}