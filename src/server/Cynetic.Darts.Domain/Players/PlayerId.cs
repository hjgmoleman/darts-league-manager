using System;
using Cynetic.Darts.Core.Types;

namespace Cynetic.Darts.Domain.Players
{
    public record PlayerId(Guid Value)
    {
        public static PlayerId New => new (CombGuid.New);
        
        public static implicit operator Guid(PlayerId id) => id.Value;
    }
}