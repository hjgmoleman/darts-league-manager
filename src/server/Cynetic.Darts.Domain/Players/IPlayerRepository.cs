using System.Threading;
using System.Threading.Tasks;
using Cynetic.Darts.Domain.Common;

namespace Cynetic.Darts.Domain.Players;

public interface IPlayerRepository : IRepository<Player, PlayerId>
{
    void AddPlayer(Player player);
    Task SaveChangesAsync(CancellationToken cancellationToken = default);
}