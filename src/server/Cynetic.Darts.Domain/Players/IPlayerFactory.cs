namespace Cynetic.Darts.Domain.Players;

public interface IPlayerFactory
{
    Player CreatePlayer(PlayerName name);
}