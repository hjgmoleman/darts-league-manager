namespace Cynetic.Darts.Domain.Players.Events
{
    public sealed record PlayerRegistered(PlayerName PlayerName);
}