namespace Cynetic.Darts.Domain.Tournaments;

public interface ITournamentFactory
{
    Tournament CreateTournament(string title);
}