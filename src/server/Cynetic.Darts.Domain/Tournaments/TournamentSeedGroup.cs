namespace Cynetic.Darts.Domain.Tournaments;

public record TournamentSeedGroup(string Name)
{
    public static TournamentSeedGroup Default = new TournamentSeedGroup(string.Empty);
}