using System;
using Cynetic.Darts.Core.Types;
using JetBrains.Annotations;

namespace Cynetic.Darts.Domain.Tournaments;

[PublicAPI]
public record TournamentRoundId(Guid Value)
{
    public static TournamentRoundId New => new (CombGuid.New);
    public static implicit operator Guid(TournamentRoundId id) => id.Value;
}