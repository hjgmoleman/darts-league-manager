using System;
using Cynetic.Darts.Core.Types;
using JetBrains.Annotations;

namespace Cynetic.Darts.Domain.Tournaments;

[PublicAPI]
public record TournamentSeedId(Guid Value)
{
    public static TournamentSeedId New => new (CombGuid.New);
    public static implicit operator Guid(TournamentSeedId id) => id.Value;
}