namespace Cynetic.Darts.Domain.Tournaments;

public class PoolTournamentRound : TournamentRound
{
    public int NumberOfGroups { get; }

    public PoolTournamentRound(int numberOfGroups)
        : base(TournamentRoundId.New)
    {
        NumberOfGroups = numberOfGroups;
    }
}