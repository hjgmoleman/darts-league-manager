using System.Collections.Generic;
using Cynetic.Darts.Domain.Common;
using JetBrains.Annotations;

namespace Cynetic.Darts.Domain.Tournaments;

[PublicAPI]
public class Tournament : AggregateRoot<TournamentId>
{
    private List<TournamentRound> _rounds = new ();

    public Tournament(string title)
        : this(TournamentId.New, title)
    {
        Emit(new TournamentRegistered(this.Id, title));
    }
    
    public Tournament(TournamentId id, string title)
        :base(id)
    {
        Title = title;
    }

    public string Title { get; private set; }

    public IReadOnlyList<TournamentRound> Rounds => _rounds.AsReadOnly();

    public void AddRound(TournamentRound round)
    {
        // set order
        round.Order(this.Rounds.Count + 1);
        _rounds.Add(round);
        
        Emit(new TournamentRoundAdded(this.Id, round.Id));
    }

    public void ChangeTitle(string title)
    {
        if (Title == title)
        {
            return;
        }
        
        Title = title;
        Emit(new TournamentTitleChanged(this.Id, title));
    }
}