using System;
using Cynetic.Darts.Core.Types;
using JetBrains.Annotations;

namespace Cynetic.Darts.Domain.Tournaments;

[PublicAPI]
public record TournamentId(Guid Value)
{
    public static TournamentId New => new (CombGuid.New);
    public static implicit operator Guid(TournamentId id) => id.Value;

    public static TournamentId From(string id)
    {
        return From(Guid.Parse(id));
    }
    
    public static TournamentId From(Guid id)
    {
        return new TournamentId(id);
    }
}