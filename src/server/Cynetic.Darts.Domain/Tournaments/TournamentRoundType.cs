using JetBrains.Annotations;

namespace Cynetic.Darts.Domain.Tournaments;

[PublicAPI]
public enum TournamentRoundType
{
    FreeForAll = 0,
    Pool = 1,
    SingleElimination = 2
}