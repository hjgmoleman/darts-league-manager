using System;
using Cynetic.Darts.Domain.Common;
using Cynetic.Darts.Domain.Players;
using JetBrains.Annotations;

namespace Cynetic.Darts.Domain.Tournaments;

[PublicAPI]
public class TournamentSeed : Entity<TournamentSeedId>
{
    public TournamentSeed( Seed seed, TournamentSeedGroup group)
        :this(TournamentSeedId.New, seed, group)
    {
    }
    
    public TournamentSeed(TournamentSeedId id, Seed seed, TournamentSeedGroup group)
        :base(id)
    {
        Seed = seed;
        Group = group;
    }
    
    public Seed Seed { get; private set; }
    public PlayerId? PlayerId { get; private set; }
    public TournamentSeedGroup Group { get; }

    public void AssignPlayer(Player player)
    {
        if (PlayerId is not null && PlayerId != player.Id)
        {
            throw TournamentSeedAlreadyAssignedException.Create(Seed);
        }
        
        PlayerId = player.Id;
    }

    public void Clear()
    {
        PlayerId = null;
    }
}