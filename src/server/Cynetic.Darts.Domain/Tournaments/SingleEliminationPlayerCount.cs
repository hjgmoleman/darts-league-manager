using System;
using System.Collections.Generic;

namespace Cynetic.Darts.Domain.Tournaments;

public record SingleEliminationPlayerCount(int Count)
{
    public static readonly SingleEliminationPlayerCount Two = new SingleEliminationPlayerCount(2);
    public static readonly SingleEliminationPlayerCount Four = new SingleEliminationPlayerCount(4);
    public static readonly SingleEliminationPlayerCount Eight = new SingleEliminationPlayerCount(8);
    public static readonly SingleEliminationPlayerCount Sixteen = new SingleEliminationPlayerCount(16);
    public static readonly SingleEliminationPlayerCount ThirtyTwo = new SingleEliminationPlayerCount(32);

    public static SingleEliminationPlayerCount Create(int playerCount)
    {
        foreach (var count in All)
        {
            if (playerCount <= count.Count)
            {
                return count;
            }
        }
        
        throw new ArgumentOutOfRangeException(nameof(playerCount));
    }
    
    public static IEnumerable<SingleEliminationPlayerCount> All 
    {
        get
        {
            yield return Two;
            yield return Four;
            yield return Eight;
            yield return Sixteen;
            yield return ThirtyTwo;
        }
    }
}