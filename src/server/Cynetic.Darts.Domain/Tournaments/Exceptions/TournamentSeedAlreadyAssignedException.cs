using System;
using Cynetic.Darts.Domain.Common;

namespace Cynetic.Darts.Domain.Tournaments;

public sealed class TournamentSeedAlreadyAssignedException : DomainException
{
    public TournamentSeedAlreadyAssignedException()
        : base("Seed is already taken.")
    {
    }
    
    public TournamentSeedAlreadyAssignedException(string message)
        : base(message)
    {
    }

    public TournamentSeedAlreadyAssignedException(string message, Exception innerException)
        : base(message, innerException)
    {
    }
    
    public static TournamentSeedAlreadyAssignedException Create(Seed seed)
     => new TournamentSeedAlreadyAssignedException($"Seed {seed.Number} is already taken.");
}