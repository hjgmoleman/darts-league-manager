namespace Cynetic.Darts.Domain.Tournaments;

public sealed record TournamentRegistered(TournamentId Id, string Title);