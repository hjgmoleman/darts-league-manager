namespace Cynetic.Darts.Domain.Tournaments;

public record TournamentTitleChanged(TournamentId TournamentId, string Title);