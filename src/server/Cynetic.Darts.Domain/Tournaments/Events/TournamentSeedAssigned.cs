using Cynetic.Darts.Domain.Players;

namespace Cynetic.Darts.Domain.Tournaments;

public record TournamentSeedAssigned(TournamentRoundId TournamentRoundId, Seed Seed, PlayerId PlayerId);