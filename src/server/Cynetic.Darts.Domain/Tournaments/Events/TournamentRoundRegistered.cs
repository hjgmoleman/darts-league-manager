namespace Cynetic.Darts.Domain.Tournaments;

public sealed record TournamentRoundRegistered(TournamentRoundId TournamentRoundId);