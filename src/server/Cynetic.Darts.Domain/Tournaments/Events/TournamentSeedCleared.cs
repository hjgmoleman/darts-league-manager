namespace Cynetic.Darts.Domain.Tournaments;

public record TournamentSeedCleared(TournamentRoundId TournamentRoundId, Seed Seed);