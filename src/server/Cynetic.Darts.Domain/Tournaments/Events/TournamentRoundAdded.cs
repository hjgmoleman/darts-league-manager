namespace Cynetic.Darts.Domain.Tournaments;

public record TournamentRoundAdded(TournamentId TournamentId, TournamentRoundId TournamentRoundId);