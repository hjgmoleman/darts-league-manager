namespace Cynetic.Darts.Domain.Tournaments;

public class FreeForAllTournamentRound : TournamentRound
{
    public FreeForAllTournamentRound()
        : base(TournamentRoundId.New)
    {
    }
}