using System;

namespace Cynetic.Darts.Domain.Tournaments;

public record Seed
{
    public int Number { get; }

    public Seed(int number)
    {
        if (number < 1)
        {
            throw new ArgumentOutOfRangeException(nameof(number), "A seed has a value of 1 or higher");
        }
        
        Number = number;
    }
};