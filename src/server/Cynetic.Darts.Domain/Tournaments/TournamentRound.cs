using System;
using System.Collections.Generic;
using System.Linq;
using Cynetic.Darts.Domain.Common;
using Cynetic.Darts.Domain.Players;
using JetBrains.Annotations;

namespace Cynetic.Darts.Domain.Tournaments;

[PublicAPI]
public abstract class TournamentRound : AggregateRoot<TournamentRoundId>
{
    private List<TournamentSeed> _seeds = new();
    private string _tournamentId = string.Empty;

    protected TournamentRound(TournamentRoundId id)
        :base(id)
    {
        Emit(new TournamentRoundRegistered(Id));
    }
    
    public int SequenceNumber { get; protected set; } = 1;
    
    public void Order(int sequenceNumber)
    {
        SequenceNumber = sequenceNumber;
    }
    
    public virtual void Seed(Seed seed, Player player)
    {
        // player already has a seed?
        if (!HasRegisteredSeed(seed))
        {
            AddSeed(this.CreateTournamentSeed(seed));    
        }
        
        var tournamentSeed = GetTournamentSeed(seed);
        tournamentSeed.AssignPlayer(player);
        
        Emit(new TournamentSeedAssigned(Id, seed, player.Id));
    }

    public void ClearSeed(Seed seed)
    {
        if (!HasRegisteredSeed(seed))
        {
            return;
        }
        
        var tournamentSeed = GetTournamentSeed(seed);
        tournamentSeed.Clear();
            
        Emit(new TournamentSeedCleared(Id, seed));
    }
    
    public PlayerId? GetPlayer(Seed seed)
    {
        if (!HasRegisteredSeed(seed))
        {
            return null;
        }
        
        var tournamentSeed = GetTournamentSeed(seed);
        return tournamentSeed.PlayerId;
    }

    protected TournamentSeed GetTournamentSeed(Seed seed)
    {
        return _seeds.Single(x => x.Seed == seed);
    }

    protected bool HasRegisteredSeed(Seed seed)
    {
        return _seeds.Any(s => s.Seed == seed);
    }

    protected virtual TournamentSeed CreateTournamentSeed(Seed seed)
    {
        return new TournamentSeed(seed, TournamentSeedGroup.Default);
    }

    private void AddSeed(TournamentSeed seed)
    {
        _seeds.Add(seed);
    }
}