using System.Threading;
using System.Threading.Tasks;
using Cynetic.Darts.Domain.Common;

namespace Cynetic.Darts.Domain.Tournaments;

public interface ITournamentRepository: IRepository<Tournament, TournamentId>
{
    void AddTournament(Tournament tournament);

    Task SaveChangesAsync(CancellationToken cancellationToken = default);
}