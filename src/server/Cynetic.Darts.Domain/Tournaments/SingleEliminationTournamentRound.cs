namespace Cynetic.Darts.Domain.Tournaments;

public class SingleEliminationTournamentRound : TournamentRound
{
    public SingleEliminationTournamentRound(SingleEliminationPlayerCount playerCount) 
        : base(TournamentRoundId.New)
    {
        PlayerCount = playerCount;
    }
    
    public SingleEliminationPlayerCount PlayerCount { get; }
}