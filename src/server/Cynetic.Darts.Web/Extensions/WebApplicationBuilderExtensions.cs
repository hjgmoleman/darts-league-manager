using Cynetic.Darts.Application.Common;
using Cynetic.Darts.Application.Players;
using Cynetic.Darts.Application.Tournaments;
using Cynetic.Darts.Domain.Players;
using Cynetic.Darts.Domain.Tournaments;
using Cynetic.Darts.GraphQL;
using Cynetic.Darts.Infrastructure.Database;
using Cynetic.Darts.Infrastructure.Database.Repositories;
using Cynetic.Darts.Infrastructure.Services;
using Cynetic.Darts.Web.Models;
using Cynetic.Darts.Web.Security;
using HotChocolate.Types.Pagination;
using MediatR;
using Microsoft.EntityFrameworkCore;
using RequestExecutorBuilderExtensions = Cynetic.Darts.GraphQL.RequestExecutorBuilderExtensions;
using UserInfoService = Cynetic.Darts.Infrastructure.Services.UserInfoService;

namespace Cynetic.Darts.Web.Extensions;

public static class WebApplicationBuilderExtensions
{
    private const string ConnectionStringKey = "DefaultConnection";
    
    public static WebApplicationBuilder ConfigureBuilder(this WebApplicationBuilder builder)
    {
        builder.ConfigureApplicationServices();
        builder.ConfigureDataServices();
        builder.ConfigureAuthServices();
        builder.ConfigurePresentationServices();
        builder.ConfigureDomainServices();
        
        return builder;
    }

    private static WebApplicationBuilder ConfigureApplicationServices(this WebApplicationBuilder builder)
    {
        builder.Services.AddTransient<IDateTimeService, SystemDateTimeService>();
        builder.Services.AddMediatR(typeof(IDateTimeService), typeof(RequestExecutorBuilderExtensions));
        
        return builder;
    }
    
    private static WebApplicationBuilder ConfigureDataServices(this WebApplicationBuilder builder)
    {
        // add data services
        var connectionString = builder.Configuration.GetConnectionString(ConnectionStringKey);
            
        builder.Services.AddDatabaseDeveloperPageExceptionFilter();
        builder.Services.AddScoped<IDbConnectionFactory>(_ => new DbConnectionFactory(connectionString));

        builder.Services.AddScoped<ITournamentRepository, TournamentRepository>();
        builder.Services.AddScoped<IPlayerRepository, PlayerRepository>();
        
        return builder;
    }

    private static WebApplicationBuilder ConfigureDomainServices(this WebApplicationBuilder builder)
    {
        builder.Services.AddSingleton<ITournamentFactory, TournamentFactory>();
        builder.Services.AddSingleton<IPlayerFactory, PlayerFactory>();
        
        return builder;
    }
    
    private static WebApplicationBuilder ConfigureAuthServices(this WebApplicationBuilder builder)
    {
        var connectionString = builder.Configuration.GetConnectionString(ConnectionStringKey);
        
        // add authentication Services
        builder.Services.AddScoped<IUserInfoService, UserInfoService>();
        builder.Services.AddDbContext<AuthDbContext>(options =>
            options.UseSqlite(connectionString));

        builder.Services.AddDbContext<DartsDbContext>(options =>
            options.UseSqlite(connectionString));

        builder.Services.AddDefaultIdentity<ApplicationUser>(options => options.SignIn.RequireConfirmedAccount = true)
               .AddEntityFrameworkStores<AuthDbContext>();

        builder.Services.AddAuthentication()
               .AddCookie();

        builder.Services.ConfigureApplicationCookie(options =>
        {
            options.Cookie.IsEssential = true;
            options.Cookie.SameSite = SameSiteMode.None;
            options.Cookie.SecurePolicy = CookieSecurePolicy.SameAsRequest;

#if DEBUG
            options.ExpireTimeSpan = TimeSpan.FromDays(7);
#else
    options.ExpireTimeSpan = TimeSpan.FromHours(1);
#endif

            options.LoginPath = "/Identity/Account/Login";
            options.AccessDeniedPath = "/Identity/Account/AccessDenied";
            options.SlidingExpiration = true;
        });

        return builder;
    }
    
    private static WebApplicationBuilder ConfigurePresentationServices(this WebApplicationBuilder builder)
    {
        builder.Services.AddControllersWithViews();
        builder.Services.AddRazorPages();
        builder.Services.AddGraphQLServer()
               .AllowIntrospection(true)
               .AddQueryType()
               .AddMutationType()
               .AddGlobalObjectIdentification()
               .AddQueryFieldToMutationPayloads()
               .AddDartsSchema()
               .SetPagingOptions(new PagingOptions()
               {
                   DefaultPageSize = 10,
                   IncludeTotalCount = true,
                   MaxPageSize = 100
               })
               //This Below is the initializer to be added...
               //NOTE: This Adds Sorting & Paging providers/conventions by default!  Do not AddPaging() & 
               //      AddSorting() in addition to '.AddPreProcessedResultsExtensions()', or the HotChocolate 
               //      Pipeline will not work as expected!
               .AddPreProcessedResultsExtensions()
               ;
    
        builder.Services.AddMiniProfiler(options =>
        {
            options.RouteBasePath = "/profiler";
        });

        return builder;
    }
}