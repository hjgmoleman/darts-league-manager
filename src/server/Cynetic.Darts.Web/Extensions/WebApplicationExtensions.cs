namespace Cynetic.Darts.Web.Extensions;

public static class WebApplicationExtensions
{
    public static WebApplication ConfigureApplication(this WebApplication app)
    {
        // Configure the HTTP request pipeline.
        if (app.Environment.IsDevelopment())
        {
            app.UseMigrationsEndPoint();
        }
        else
        {
            // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
            app.UseHsts();
            app.UseHttpsRedirection();
        }
        
        app.UseCors(cors =>
        {
            cors.WithMethods(HttpMethods.Get, HttpMethods.Post, HttpMethods.Options);
            cors.WithOrigins("http://localhost:3000", "http://localhost:3001");
            cors.AllowAnyHeader();
            cors.AllowCredentials();
        });

        app.UseStaticFiles();
        app.UseRouting();

        app.UseAuthentication();
        app.UseAuthorization();

        app.UseMiniProfiler();
        app.MapGraphQL();

        app.MapControllerRoute(
            name: "default",
            pattern: "{controller}/{action=Index}/{id?}");
        app.MapRazorPages();

        app.MapFallbackToFile("index.html");
        
        return app;
    }
}