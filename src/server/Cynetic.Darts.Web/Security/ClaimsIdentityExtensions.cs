using System.Security.Claims;

namespace Cynetic.Darts.Web.Security;

public static class ClaimsIdentityExtensions
{
    public static string? GetUserName(this ClaimsIdentity identity)
    {
        return identity.GetClaimValue(ClaimTypes.Name);
    }

    public static string? GetUserId(this ClaimsIdentity identity)
    {
        return identity.GetClaimValue(ClaimTypes.NameIdentifier);
    }

    public static string? GetEmail(this ClaimsIdentity identity)
    {
        return identity.GetClaimValue(ClaimTypes.Email);
    }

    public static string[]? GetRoles(this ClaimsIdentity identity)
    {
        return identity.GetClaimValues(ClaimTypes.Role);
    }

    private static string? GetClaimValue(this ClaimsIdentity identity, string claimType)
    {
        var claim = identity.FindFirst(claimType);
        return claim?.Value;
    }

    private static string[]? GetClaimValues(this ClaimsIdentity identity, string claimType)
    {
        var claims = identity.FindAll(claimType);
        return claims?.Select(x => x.Value).ToArray();
    }
}