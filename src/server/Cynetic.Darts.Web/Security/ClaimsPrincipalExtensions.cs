using System.Security.Claims;

namespace Cynetic.Darts.Web.Security;

public static class ClaimsPrincipalExtensions
{
    public static string? GetUserName(this ClaimsPrincipal principal)
    {
        return principal.GetClaimValue(ClaimTypes.Name);
    }

    public static string? GetUserId(this ClaimsPrincipal principal)
    {
        return principal.GetClaimValue(ClaimTypes.NameIdentifier);
    }

    public static string? GetEmail(this ClaimsPrincipal principal)
    {
        return principal.GetClaimValue(ClaimTypes.Email);
    }

    public static string[]? GetRoles(this ClaimsPrincipal principal)
    {
        return principal.GetClaimValues(ClaimTypes.Role);
    }
    
    private static string? GetClaimValue(this ClaimsPrincipal principal, string claimType)
    {
        var claim = principal.FindFirst(claimType);
        return claim?.Value;
    }

    private static string[]? GetClaimValues(this ClaimsPrincipal principal, string claimType)
    {
        var claims = principal.FindAll(claimType);
        return claims?.Select(x => x.Value).ToArray();
    }
}