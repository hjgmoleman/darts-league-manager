using Cynetic.Darts.Application.Common;

namespace Cynetic.Darts.Web.Security;

public class UserInfoService : IUserInfoService
{
    private readonly IHttpContextAccessor contextAccessor;

    public UserInfoService(IHttpContextAccessor contextAccessor)
    {
        this.contextAccessor = contextAccessor;
    }
    
    public Task<UserInfo?> GetUserInfoAsync()
    {
        var currentUser = this.contextAccessor.HttpContext?.User;
        if (currentUser is not null)
        {
            var user = new UserInfo(currentUser.GetUserId() ?? string.Empty);
            return Task.FromResult<UserInfo?>(user);
        }

        return Task.FromResult<UserInfo?>(null);
    }
}