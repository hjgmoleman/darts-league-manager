﻿using Microsoft.AspNetCore.Identity;

namespace Cynetic.Darts.Web.Models;

public class ApplicationUser : IdentityUser
{
}