using HotChocolate.Types.Relay;

namespace Cynetic.Darts.GraphQL.Players.Queries;

public class Player
{   
    [ID("Player")]
    public Guid Id { get; set; }    
}