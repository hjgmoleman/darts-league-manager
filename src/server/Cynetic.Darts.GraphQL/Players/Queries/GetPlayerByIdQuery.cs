using Cynetic.Darts.Infrastructure.Database;
using Dapper;
using HotChocolate;
using HotChocolate.Types;
using HotChocolate.Types.Relay;
using JetBrains.Annotations;

namespace Cynetic.Darts.GraphQL.Players.Queries;

[ExtendObjectType(OperationTypeNames.Query)]
[UsedImplicitly]
public class GetPlayerByIdQuery
{
    private const string CommandText = @"
            SELECT Player.Id
                 , Player.Name
                FROM Player
               WHERE Player.Id = @Id
        ";
    
    [UsedImplicitly]
    public async Task<Player> GetPlayerAsync([ID("Player")] Guid playerId, [Service] IDbConnectionFactory connectionFactory)
    {
        using var connection = connectionFactory.GetDatabaseConnection();
        return await connection.QuerySingleAsync<Player>(CommandText, new
        {
            Id = playerId
        });
    }
}