using Cynetic.Darts.Infrastructure.Database;
using Dapper;
using HotChocolate;
using HotChocolate.Types;

namespace Cynetic.Darts.GraphQL.Players.Queries;

[ExtendObjectType(OperationTypeNames.Query)]
public class GetPlayersQuery
{
    private const string CommandText = @"
            SELECT Player.Id
                 , Player.Name
                FROM Player
        "; 
    
    public async Task<IEnumerable<Player>> GetPlayersAsync([Service] IDbConnectionFactory connectionFactory)
    {
        using var connection = connectionFactory.GetDatabaseConnection();
        return await connection.QueryAsync<Player>(CommandText);
    }
}