using Cynetic.Darts.GraphQL.Tournaments.Queries.Handlers;
using HotChocolate;
using HotChocolate.Types;
using HotChocolate.Types.Relay;
using JetBrains.Annotations;
using MediatR;

namespace Cynetic.Darts.GraphQL.Tournaments.Queries;

[ExtendObjectType(OperationTypeNames.Query)]
public class QueryTournamentResolvers
{
    [UsedImplicitly]
    public async Task<Tournament?> GetTournament(
        [ID(nameof(Tournament))] string id,
        [Service]ISender mediator)
    {
        var result = await mediator.Send(new GetTournamentById(id));
        
        return result.ValueOrDefault;
    }
    
    [UsedImplicitly]
    public async Task<IEnumerable<Tournament>> GetTournaments([Service]ISender mediator)
    {
        var result = await mediator.Send(new GetTournamentList());
        
        return result.ValueOrDefault;
    }
}