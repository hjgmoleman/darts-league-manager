using Cynetic.Darts.GraphQL.Tournaments.Queries.Handlers;
using HotChocolate;
using HotChocolate.PreProcessingExtensions;
using HotChocolate.Types.Relay;
using JetBrains.Annotations;
using MediatR;

namespace Cynetic.Darts.GraphQL.Tournaments.Queries;

[Node]
public class Tournament
{
    [ID]
    public string Id { get; set; }
    public string Title { get; set; } = string.Empty;
    
    [PreProcessingParentDependencies(nameof(Id))]
    [UsedImplicitly]
    public async Task<IEnumerable<TournamentRound>> GetRounds(
        [Parent] Tournament parent,
        [Service]ISender mediator)
    {
        var result = await mediator.Send(new GetTournamentRoundList(parent.Id));
        return result.ValueOrDefault;
    }
    
    public static async Task<Tournament> GetAsync(
        string id,
        [Service]ISender mediator)
    {
        var result = await mediator.Send(new GetTournamentById(id));
        return result.ValueOrDefault;
    }
}