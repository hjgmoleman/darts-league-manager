using HotChocolate.Types.Relay;

namespace Cynetic.Darts.GraphQL.Tournaments.Queries;

public class TournamentRound
{
    [ID]
    public string Id { get; set; }
    public int SequenceNumber { get; set; }
    public int RoundType { get; set; }
    public int NumberOfGroups { get; set; }
    public int PlayerCount { get; set; }
}