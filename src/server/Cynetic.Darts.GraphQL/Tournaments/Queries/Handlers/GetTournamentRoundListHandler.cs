using Cynetic.Darts.Infrastructure.Database;
using Dapper;
using FluentResults;
using MediatR;

namespace Cynetic.Darts.GraphQL.Tournaments.Queries.Handlers;

public class GetTournamentRoundListHandler : IRequestHandler<GetTournamentRoundList, Result<IReadOnlyList<TournamentRound>>>
{
    private const string CommandText = @"
        SELECT Id, SequenceNumber, RoundType, NumberOfGroups, PlayerCount 
        FROM TournamentRound 
        WHERE TournamentId = @TournamentId
    ";

    private readonly IDbConnectionFactory _connectionFactory;

    public GetTournamentRoundListHandler(IDbConnectionFactory connectionFactory)
    {
        _connectionFactory = connectionFactory;
    }
    
    public async Task<Result<IReadOnlyList<TournamentRound>>> Handle(GetTournamentRoundList request, CancellationToken cancellationToken)
    {
        var data = await this.GetTournamentRounds(request.TournamentId);
        return Result.Ok(data);
    }

    private async Task<IReadOnlyList<TournamentRound>> GetTournamentRounds(string tournamentId)
    {
        using var connection = _connectionFactory.GetDatabaseConnection();
        var data = await connection.QueryAsync<TournamentRound>(CommandText,
            new
            {
                TournamentId = tournamentId
            });

        return data.ToList();
    }
}