using Cynetic.Darts.Infrastructure.Database;
using Dapper;
using FluentResults;
using MediatR;

namespace Cynetic.Darts.GraphQL.Tournaments.Queries.Handlers;

public class GetTournamentByIdHandler : IRequestHandler<GetTournamentById, Result<Tournament>>
{
    private readonly IDbConnectionFactory _connectionFactory;
    private const string CommandText = "SELECT Id, Title FROM Tournament WHERE Id = @Id";

    public GetTournamentByIdHandler(IDbConnectionFactory connectionFactory)
    {
        _connectionFactory = connectionFactory;
    }

    public async Task<Result<Tournament>> Handle(GetTournamentById request, CancellationToken cancellationToken)
    {
        using var connection = _connectionFactory.GetDatabaseConnection();
        var tournament = await connection.QueryFirstAsync<Tournament>(CommandText, new
        {
            Id = request.Id
        });

        return Result.Ok(tournament);
    }
}