using FluentResults;
using MediatR;

namespace Cynetic.Darts.GraphQL.Tournaments.Queries.Handlers;

public record GetTournamentById(string Id) : IRequest<Result<Tournament>>;