using FluentResults;
using MediatR;

namespace Cynetic.Darts.GraphQL.Tournaments.Queries.Handlers;

public record GetTournamentList : IRequest<Result<IReadOnlyList<Tournament>>>;