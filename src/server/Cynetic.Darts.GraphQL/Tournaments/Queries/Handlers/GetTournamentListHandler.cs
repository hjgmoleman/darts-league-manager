using Cynetic.Darts.Infrastructure.Database;
using Dapper;
using FluentResults;
using MediatR;

namespace Cynetic.Darts.GraphQL.Tournaments.Queries.Handlers;

public class GetTournamentListHandler : IRequestHandler<GetTournamentList, Result<IReadOnlyList<Tournament>>>
{
    private const string CommandText = "SELECT Id, Title FROM Tournament";
    private readonly IDbConnectionFactory _connectionFactory;

    public GetTournamentListHandler(IDbConnectionFactory connectionFactory)
    {
        _connectionFactory = connectionFactory;
    }

    public async Task<Result<IReadOnlyList<Tournament>>> Handle(GetTournamentList request, CancellationToken cancellationToken)
    {
        using var connection = _connectionFactory.GetDatabaseConnection();
        var list = await connection.QueryAsync<Tournament>(CommandText);
        
        return Result.Ok<IReadOnlyList<Tournament>>(list.ToList());
    }
}