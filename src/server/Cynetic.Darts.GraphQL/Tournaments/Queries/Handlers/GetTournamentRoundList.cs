using FluentResults;
using MediatR;

namespace Cynetic.Darts.GraphQL.Tournaments.Queries.Handlers;

public record GetTournamentRoundList(string TournamentId) : IRequest<Result<IReadOnlyList<TournamentRound>>>;