using Cynetic.Darts.GraphQL.Tournaments.Queries;
using HotChocolate.Types.Relay;
using JetBrains.Annotations;

namespace Cynetic.Darts.GraphQL.Tournaments.Mutations;

[UsedImplicitly]
public record UpdateTournamentInput
{
    [ID(nameof(Tournament))] 
    public string Id { get; set; } = string.Empty;
    public string Title { get; set; } = string.Empty;
}