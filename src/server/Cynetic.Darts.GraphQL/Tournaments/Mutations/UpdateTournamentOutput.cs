namespace Cynetic.Darts.GraphQL.Tournaments.Mutations;

public record UpdateTournamentOutput
{
    public string? Message { get; set; }
    public bool Success { get; set; }
}