using Cynetic.Darts.Application.Tournaments.UseCases;
using HotChocolate;
using HotChocolate.Types;
using JetBrains.Annotations;
using MediatR;

namespace Cynetic.Darts.GraphQL.Tournaments.Mutations;

[ExtendObjectType(OperationTypeNames.Mutation)]
[UsedImplicitly]
public class MutationTournamentResolvers
{
    [UsedImplicitly]
    public async Task<UpdateTournamentOutput> UpdateTournament(
        UpdateTournamentInput data,
        [Service] ISender mediator)
    {
        var command = new UpdateTournament.Command(data.Id, data.Title);
        try
        {
            await mediator.Send(command);
            return new UpdateTournamentOutput
            {
                Success = true
            };
        }
        catch (Exception e)
        {
            return new UpdateTournamentOutput
            {
                Success = false,
                Message = e.ToString()
            };
        }
    }
}