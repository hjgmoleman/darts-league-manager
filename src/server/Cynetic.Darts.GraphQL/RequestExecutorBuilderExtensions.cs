using Cynetic.Darts.GraphQL.Players.Queries;
using Cynetic.Darts.GraphQL.Tournaments.Mutations;
using Cynetic.Darts.GraphQL.Tournaments.Queries;
using HotChocolate.Execution.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Cynetic.Darts.GraphQL;

public static class RequestExecutorBuilderExtensions
{
    public static IRequestExecutorBuilder AddDartsSchema(this IRequestExecutorBuilder builder)
    {
        return builder
               .AddPlayerResolvers()
               .AddTournamentResolvers();
    }

    private static IRequestExecutorBuilder AddPlayerResolvers(this IRequestExecutorBuilder builder)
    {
        return builder
               .AddTypeExtension<GetPlayersQuery>()
               .AddTypeExtension<GetPlayerByIdQuery>();
    }

    private static IRequestExecutorBuilder AddTournamentResolvers(this IRequestExecutorBuilder builder)
    {
        return builder
               .AddTypeExtension<QueryTournamentResolvers>()
               .AddTypeExtension<MutationTournamentResolvers>()
               .AddType<Tournament>()
               .AddType<TournamentRound>();
    }
}