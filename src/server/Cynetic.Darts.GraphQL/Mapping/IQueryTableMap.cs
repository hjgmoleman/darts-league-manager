namespace Cynetic.Darts.GraphQL.Mapping;

public interface IQueryTableMap<T>
{
    TableName TableName { get; }
    string GetColumnName(string field);
}