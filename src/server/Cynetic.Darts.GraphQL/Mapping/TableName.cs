namespace Cynetic.Darts.GraphQL.Mapping;

public record TableName(string Name);