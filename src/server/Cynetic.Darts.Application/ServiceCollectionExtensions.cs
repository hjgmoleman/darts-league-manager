using Cynetic.Darts.Application.Players;
using Cynetic.Darts.Application.Tournaments;
using Cynetic.Darts.Domain.Players;
using Cynetic.Darts.Domain.Tournaments;
using MediatR;
using Microsoft.Extensions.DependencyInjection;

namespace Cynetic.Darts.Application;

public static class ServiceCollectionExtensions
{
    public static IServiceCollection AddApplicationServices(this IServiceCollection services)
    {
        services.AddMediatR(new []
        {
            typeof(ServiceCollectionExtensions).Assembly
        });

        services.AddDomainFactoryImplementations();
        
        return services;
    }

    public static IServiceCollection AddDomainFactoryImplementations(this IServiceCollection services)
    {
        return services
               .AddTransient<IPlayerFactory, PlayerFactory>()
               .AddTransient<ITournamentFactory, TournamentFactory>();
    }
}