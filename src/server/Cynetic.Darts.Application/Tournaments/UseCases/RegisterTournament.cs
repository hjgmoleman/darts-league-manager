using System;
using System.Threading;
using System.Threading.Tasks;
using Cynetic.Darts.Domain.Tournaments;
using JetBrains.Annotations;
using MediatR;

namespace Cynetic.Darts.Application.Tournaments.UseCases;

public static class RegisterTournament
{
    public record Command(string Title) : IRequest<Guid>;

    [UsedImplicitly]
    public class Handler : IRequestHandler<Command, Guid>
    {
        private readonly ITournamentRepository _repository;
        private readonly ITournamentFactory _factory;

        public Handler(ITournamentRepository repository, ITournamentFactory factory)
        {
            _repository = repository;
            _factory = factory;
        }

        public async Task<Guid> Handle(Command request, CancellationToken cancellationToken)
        {
            var tournament = this._factory.CreateTournament(request.Title);
            _repository.AddTournament(tournament);

            await _repository.SaveChangesAsync(cancellationToken);

            return tournament.Id;
        }
    }
}