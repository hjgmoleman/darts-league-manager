using System.Threading;
using System.Threading.Tasks;
using Cynetic.Darts.Domain.Tournaments;
using MediatR;

namespace Cynetic.Darts.Application.Tournaments.UseCases;

public static class UpdateTournament
{
    public record Command(string Id, string Title) : IRequest<Unit>;

    public class Handler : IRequestHandler<Command, Unit>
    {
        private readonly ITournamentRepository _repository;

        public Handler(ITournamentRepository repository)
        {
            _repository = repository;
        }

        public async Task<Unit> Handle(Command request, CancellationToken cancellationToken)
        {
            var tournament = await _repository.GetAsync(TournamentId.From(request.Id), cancellationToken);
            tournament.ChangeTitle(request.Title);

            await _repository.SaveChangesAsync(cancellationToken);

            return Unit.Value;
        }
    }
}