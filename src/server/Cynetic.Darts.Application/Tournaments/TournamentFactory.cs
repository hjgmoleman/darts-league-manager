using Cynetic.Darts.Domain.Tournaments;

namespace Cynetic.Darts.Application.Tournaments;

public class TournamentFactory : ITournamentFactory
{
    public Tournament CreateTournament(string title)
    {
        return new Tournament(TournamentId.New, title);
    }
}