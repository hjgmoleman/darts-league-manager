using System;

namespace Cynetic.Darts.Application.Common;

public interface IDateTimeService
{
    DateTime NowUtc { get; }
}