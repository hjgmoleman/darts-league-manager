using System.Threading.Tasks;

namespace Cynetic.Darts.Application.Common;

public interface IUserInfoService
{
    Task<UserInfo> GetUserInfoAsync();
}