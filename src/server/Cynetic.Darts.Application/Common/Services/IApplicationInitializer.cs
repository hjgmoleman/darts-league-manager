namespace Cynetic.Darts.Application.Common;

public interface IApplicationInitializer
{
    void Initialize();
}