using System.Collections.Generic;

namespace Cynetic.Darts.Application.Common;

public interface IUserInfo
{
    string Id { get; } 
    IDictionary<string, string> Info { get; }
}