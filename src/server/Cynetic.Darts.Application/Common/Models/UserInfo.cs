using System.Collections.Generic;

namespace Cynetic.Darts.Application.Common;

public class UserInfo : IUserInfo
{
    public UserInfo(string identifier)
        :this(identifier, new Dictionary<string, string>())
    {
    }
        
    public UserInfo(string identifier, IDictionary<string, string> info)
    {
        this.Id = identifier;
        this.Info = info;
    }
        
    public string Id { get; set; } 
    public IDictionary<string, string> Info { get; set; }

    public bool IsAnonymous => string.IsNullOrWhiteSpace(this.Id);
}