using System.Threading;
using System.Threading.Tasks;
using Cynetic.Darts.Domain.Players;
using JetBrains.Annotations;
using MediatR;

namespace Cynetic.Darts.Application.Players.UseCases;

public static class RegisterPlayer
{
    public record Command(string PlayerName) : IRequest<string>;

    [UsedImplicitly]
    public class Handler : IRequestHandler<Command, string>
    {
        private readonly IPlayerRepository _repository;
        private readonly IPlayerFactory _factory;

        public Handler(IPlayerRepository repository, IPlayerFactory factory)
        {
            _repository = repository;
            _factory = factory;
        }

        public async Task<string> Handle(Command request, CancellationToken cancellationToken)
        {
            var player = _factory.CreatePlayer(new PlayerName(request.PlayerName));

            _repository.AddPlayer(player);
            await _repository.SaveChangesAsync(cancellationToken);

            return player.Id.ToString();
        }
    }
}