using Cynetic.Darts.Domain.Players;

namespace Cynetic.Darts.Application.Players;

public class PlayerFactory : IPlayerFactory
{
    public Player CreatePlayer(PlayerName name)
    {
        var player = new Player(PlayerId.New, name);
        return player;
    }
}