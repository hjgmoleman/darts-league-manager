using System;
using Cynetic.Darts.Application.Common;

namespace Cynetic.Darts.Infrastructure.Services;

public class SystemDateTimeService : IDateTimeService
{
    public DateTime NowUtc => DateTime.UtcNow;
}