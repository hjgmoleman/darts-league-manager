using System.Threading.Tasks;
using Cynetic.Darts.Application.Common;

namespace Cynetic.Darts.Infrastructure.Services;

public class UserInfoService : IUserInfoService
{
    public Task<UserInfo> GetUserInfoAsync()
    {
        var info = new UserInfo("dev");
        return Task.FromResult(info);
    }
}