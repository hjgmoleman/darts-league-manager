using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using Cynetic.Darts.Domain.Audit;
using Microsoft.EntityFrameworkCore.ChangeTracking;

namespace Cynetic.Darts.Infrastructure.Models.Audit
{
    public class AuditEntry
    {
        public AuditEntry(EntityEntry entry)
        {
            Entry = entry;
        }

        public EntityEntry Entry { get; }
        public string UserId { get; set; }
        public string TableName { get; set; }
        public Dictionary<string, object> KeyValues { get; } = new();
        public Dictionary<string, object> OldValues { get; } = new();
        public Dictionary<string, object> NewValues { get; } = new();
        public List<PropertyEntry> TemporaryProperties { get; } = new();
        public AuditType AuditType { get; set; }
        public List<string> ChangedColumns { get; } = new();
        public bool HasTemporaryProperties => TemporaryProperties.Any();

        public Audit ToAudit()
        {
            var audit = new Audit
            {
                UserId = UserId,
                Type = AuditType.Name,
                TableName = TableName,
                DateTime = DateTime.UtcNow,
                PrimaryKey = JsonSerializer.Serialize(this.KeyValues),
                OldValues = OldValues.Count == 0 ? null : JsonSerializer.Serialize(this.OldValues),
                NewValues = NewValues.Count == 0 ? null : JsonSerializer.Serialize(this.NewValues),
                AffectedColumns = ChangedColumns.Count == 0 ? null : JsonSerializer.Serialize(this.ChangedColumns)
            };
            return audit;
        }
    }
}