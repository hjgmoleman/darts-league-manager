using System;
using Cynetic.Darts.Core.Types;
using Cynetic.Darts.Domain.Common;

namespace Cynetic.Darts.Infrastructure.Models.Audit
{
    public class Audit : IEntity<Guid>
    {
        public Guid Id { get; set; } = CombGuid.New;
        public string UserId { get; set; } = string.Empty;
        public string Type { get; set; } = string.Empty;
        public string TableName { get; set; } = string.Empty;
        public DateTime DateTime { get; set; } = DateTime.Now;
        public string OldValues { get; set; } = string.Empty;
        public string NewValues { get; set; } = string.Empty;
        public string AffectedColumns { get; set; } = string.Empty;
        public string PrimaryKey { get; set; } = string.Empty;
    }
}