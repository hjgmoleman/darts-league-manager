using System.Data;

namespace Cynetic.Darts.Infrastructure.Database;

public interface IDbConnectionFactory
{
    IDbConnection GetDatabaseConnection();
}