using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Cynetic.Darts.Application.Common;
using Cynetic.Darts.Domain.Audit;
using Cynetic.Darts.Domain.Players;
using Cynetic.Darts.Domain.Tournaments;
using Microsoft.EntityFrameworkCore;

namespace Cynetic.Darts.Infrastructure.Database
{
    public class DartsDbContext : AuditableContext
    {
        private readonly IUserInfoService userInfoService;
        private readonly IDateTimeService dateTimeService;

        public DartsDbContext(DbContextOptions<DartsDbContext> options, IUserInfoService userInfoService, IDateTimeService dateTimeService) 
            : base(options)
        {
            this.userInfoService = userInfoService;
            this.dateTimeService = dateTimeService;
        }
        
        public DbSet<Player> Players { get; set; }
        public DbSet<Tournament> Tournaments { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            
            modelBuilder.Ignore<PlayerId>();
            modelBuilder.Ignore<PlayerName>();
            modelBuilder.Ignore<TournamentId>();
            modelBuilder.Ignore<TournamentRoundId>();
            modelBuilder.Ignore<Seed>();
            modelBuilder.Ignore<TournamentSeedId>();
            modelBuilder.Ignore<TournamentSeedGroup>();
            modelBuilder.Ignore<SingleEliminationPlayerCount>();
            
            modelBuilder.ApplyConfigurationsFromAssembly(typeof(DartsDbContext).Assembly);
        }
        
        public override async Task<int> SaveChangesAsync(CancellationToken cancellationToken = new())
        {
            var now = dateTimeService.NowUtc;
            var user = await userInfoService.GetUserInfoAsync();
            
            foreach (var entry in ChangeTracker.Entries<IAuditableEntity>().ToList())
            {
                switch (entry.State)
                {
                    case EntityState.Added:
                        entry.Entity.CreatedOn = now;
                        entry.Entity.CreatedBy = user?.Id;
                        break;

                    case EntityState.Modified:
                        entry.Entity.LastModifiedOn = now;
                        entry.Entity.LastModifiedBy = user?.Id;
                        break;
                }
            }

            if (user?.IsAnonymous == false)
            {
                return await base.SaveChangesAsync(user.Id);
            }
            
            return await SaveChangesAsync(cancellationToken);
        }
    }
}