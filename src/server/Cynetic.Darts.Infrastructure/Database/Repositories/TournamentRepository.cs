using System.Threading;
using System.Threading.Tasks;
using Cynetic.Darts.Domain.Tournaments;
using JetBrains.Annotations;
using Microsoft.EntityFrameworkCore;

namespace Cynetic.Darts.Infrastructure.Database.Repositories;

[UsedImplicitly]
public class TournamentRepository : EfRepository, ITournamentRepository
{
    public TournamentRepository(DartsDbContext dbContext)
        :base(dbContext)
    {
    }
    
    public async Task<Tournament> GetAsync(TournamentId id, CancellationToken cancellationToken = default)
    {
        return await DbContext.Tournaments
                         .Include("_rounds")
                         //.Include(x => x.Seeds)
                         .FirstAsync(x => x.Id == id, cancellationToken);
    }

    public void AddTournament(Tournament tournament)
    {
        DbContext.Tournaments.Add(tournament);
    }
}