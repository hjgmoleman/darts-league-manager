using System;
using System.Threading;
using System.Threading.Tasks;
using Cynetic.Darts.Domain.Players;
using JetBrains.Annotations;
using Microsoft.EntityFrameworkCore;

namespace Cynetic.Darts.Infrastructure.Database.Repositories;

[UsedImplicitly]
public class PlayerRepository : EfRepository, IPlayerRepository
{
    public PlayerRepository(DartsDbContext dbContext)
        :base(dbContext)
    {
    }
    
    public async Task<Player> GetAsync(PlayerId id, CancellationToken cancellationToken = default)
    {
        return await DbContext.Players.FirstAsync(x => x.Id == id, cancellationToken);
    }
    
    public void AddPlayer(Player player)
    {
        DbContext.Players.Add(player);
    }
}