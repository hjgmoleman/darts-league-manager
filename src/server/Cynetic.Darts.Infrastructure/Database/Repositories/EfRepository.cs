using System.Threading;
using System.Threading.Tasks;

namespace Cynetic.Darts.Infrastructure.Database.Repositories;

public abstract class EfRepository
{
    protected EfRepository(DartsDbContext dbContext)
    {
        DbContext = dbContext;
    }
    
    protected DartsDbContext DbContext { get; }
    
    public virtual async Task SaveChangesAsync(CancellationToken cancellationToken = default)
    {
        await DbContext.SaveChangesAsync(cancellationToken);
    }
}