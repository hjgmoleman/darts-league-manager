using System.Data;
using Microsoft.Data.Sqlite;

namespace Cynetic.Darts.Infrastructure.Database;

public class DbConnectionFactory : IDbConnectionFactory
{
    private readonly string _connectionString;

    public DbConnectionFactory(string connectionString)
    {
        this._connectionString = connectionString;
    }
    
    public IDbConnection GetDatabaseConnection()
    {
        return new SqliteConnection(this._connectionString);
    }
}