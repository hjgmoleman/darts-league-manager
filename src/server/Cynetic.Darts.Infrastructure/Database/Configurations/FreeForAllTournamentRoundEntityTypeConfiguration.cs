using Cynetic.Darts.Domain.Tournaments;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Cynetic.Darts.Infrastructure.Database.Configurations;

public class FreeForAllTournamentRoundEntityTypeConfiguration : IEntityTypeConfiguration<FreeForAllTournamentRound>
{
    public void Configure(EntityTypeBuilder<FreeForAllTournamentRound> builder)
    {
        builder.HasBaseType<TournamentRound>();
    }
}