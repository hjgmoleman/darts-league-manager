using Cynetic.Darts.Domain.Tournaments;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Cynetic.Darts.Infrastructure.Database.Configurations;

public class SingleEliminationTournamentRoundEntityTypeConfiguration : IEntityTypeConfiguration<SingleEliminationTournamentRound>
{
    public void Configure(EntityTypeBuilder<SingleEliminationTournamentRound> builder)
    {
        builder.HasBaseType<TournamentRound>();
        builder.Property(x => x.PlayerCount)
               .HasConversion(playerCount => playerCount.Count, value => SingleEliminationPlayerCount.Create(value));
    }
}