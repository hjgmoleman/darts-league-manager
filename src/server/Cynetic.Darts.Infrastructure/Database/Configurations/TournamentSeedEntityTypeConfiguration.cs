using Cynetic.Darts.Domain.Tournaments;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Cynetic.Darts.Infrastructure.Database.Configurations;

public class TournamentSeedEntityTypeConfiguration : IEntityTypeConfiguration<TournamentSeed>
{
    public void Configure(EntityTypeBuilder<TournamentSeed> builder)
    {
        builder.ToTable("TournamentSeed");
        builder.HasKey(x => x.Id);
        builder.Property(x => x.Id)
               .HasConversion<TournamentSeedIdConverter>();
        
        builder.Property(x => x.Seed)
               .HasConversion(label => label.Number, value => new Seed(value));

        builder.Property(x => x.Group)
               .HasConversion(group => group.Name, value => new TournamentSeedGroup(value));
    }
}