using Cynetic.Darts.Domain.Tournaments;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Cynetic.Darts.Infrastructure.Database.Configurations;

public class TournamentEntityTypeConfiguration : IEntityTypeConfiguration<Tournament>
{
    public void Configure(EntityTypeBuilder<Tournament> builder)
    {
        builder.ToTable("Tournament");

        builder.HasKey(x => x.Id);
        builder.Property(x => x.Id)
               .HasConversion(id => id.Value, value => new TournamentId(value));

        builder.Property(x => x.Title);
        builder.HasMany(x => x.Rounds)
               .WithOne();
        
        builder.Navigation(nameof(Tournament.Rounds))
               .Metadata.SetPropertyAccessMode(PropertyAccessMode.Field);
    }
}