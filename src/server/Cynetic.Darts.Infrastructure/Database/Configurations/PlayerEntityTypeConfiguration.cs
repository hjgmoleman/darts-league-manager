using Cynetic.Darts.Domain.Players;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Cynetic.Darts.Infrastructure.Database.Configurations;

public class PlayerEntityTypeConfiguration : IEntityTypeConfiguration<Player>
{
    public void Configure(EntityTypeBuilder<Player> builder)
    {
        builder.ToTable("Player");

        builder.Property(x => x.Id)
               .HasConversion<PlayerIdConverter>();

        builder.Property(x => x.Name)
               .HasConversion<PlayerNameConverter>();
    }
}