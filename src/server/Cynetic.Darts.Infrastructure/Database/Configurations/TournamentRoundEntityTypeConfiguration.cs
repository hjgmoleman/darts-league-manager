using Cynetic.Darts.Domain.Tournaments;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Cynetic.Darts.Infrastructure.Database.Configurations;

public class TournamentRoundEntityTypeConfiguration : IEntityTypeConfiguration<TournamentRound>
{
    public void Configure(EntityTypeBuilder<TournamentRound> builder)
    {
        builder.ToTable("TournamentRound");
        builder.Property(x => x.Id)
               .HasConversion<TournamentRoundIdConverter>();

        builder.HasDiscriminator<string>("RoundType")
                    .HasValue<FreeForAllTournamentRound>(TournamentRoundType.FreeForAll.ToString())
                    .HasValue<PoolTournamentRound>(TournamentRoundType.Pool.ToString())
                    .HasValue<SingleEliminationTournamentRound>(TournamentRoundType.SingleElimination.ToString());
        
        builder.Property(x => x.SequenceNumber);
        // builder.HasMany<TournamentSeed>("_seeds")
        //        .WithOne()
        //        .HasForeignKey("TournamentRoundId");
    }
}
