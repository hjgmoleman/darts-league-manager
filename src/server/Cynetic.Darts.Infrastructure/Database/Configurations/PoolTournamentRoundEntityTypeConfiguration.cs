using Cynetic.Darts.Domain.Tournaments;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Cynetic.Darts.Infrastructure.Database.Configurations;

public class PoolTournamentRoundEntityTypeConfiguration : IEntityTypeConfiguration<PoolTournamentRound>
{
    public void Configure(EntityTypeBuilder<PoolTournamentRound> builder)
    {
        builder.HasBaseType<TournamentRound>();
        builder.Property(x => x.NumberOfGroups);
    }
}