using System;
using Cynetic.Darts.Domain.Tournaments;
using JetBrains.Annotations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace Cynetic.Darts.Infrastructure.Database.Configurations;

[UsedImplicitly]
public class TournamentSeedIdConverter : ValueConverter<TournamentSeedId, Guid>
{
    public TournamentSeedIdConverter()
        :base(x=> x.Value, x => new TournamentSeedId(x))
    {
    }
}