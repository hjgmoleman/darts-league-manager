using System;
using Cynetic.Darts.Domain.Tournaments;
using JetBrains.Annotations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace Cynetic.Darts.Infrastructure.Database.Configurations;

[UsedImplicitly]
public class TournamentRoundIdConverter : ValueConverter<TournamentRoundId, Guid>
{
    public TournamentRoundIdConverter()
        : base(v => v.Value, v => new TournamentRoundId(v))
    {
    }
}