using Cynetic.Darts.Domain.Players;
using JetBrains.Annotations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace Cynetic.Darts.Infrastructure.Database.Configurations;

[UsedImplicitly]
public class PlayerNameConverter : ValueConverter<PlayerName, string>
{
    public PlayerNameConverter()
        : base(v => v.Name, name => new PlayerName(name))
    {
    }
}