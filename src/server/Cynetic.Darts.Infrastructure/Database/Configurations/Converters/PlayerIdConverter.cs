using System;
using Cynetic.Darts.Domain.Players;
using JetBrains.Annotations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace Cynetic.Darts.Infrastructure.Database.Configurations;

[UsedImplicitly]
public class PlayerIdConverter : ValueConverter<PlayerId, Guid>
{
    public PlayerIdConverter()
        :base(v => v.Value, id => new PlayerId(id))
    {
    }
}