using System;
using Cynetic.Darts.Domain.Players;
using Shouldly;
using Xunit;

namespace Cynetic.Darts.Domain.Tests.Players;

public class PlayerIdTests
{
    [Fact]
    public void ImplicitCastToGuid_ReturnsNotDefault()
    {
        var guid = Guid.NewGuid();
        var id = new PlayerId(guid);

        // act
        Guid asGuid = id;
        
        // assert
        asGuid.Equals(guid).ShouldBeTrue();
    }
}