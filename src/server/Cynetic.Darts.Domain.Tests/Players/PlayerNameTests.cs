using System;
using Cynetic.Darts.Domain.Players;
using Shouldly;
using Xunit;

namespace Cynetic.Darts.Domain.Tests.Players;

public class PlayerNameTests
{
    [Fact]
    public void Constructor_GivenEmptyString_ThrowsArgumentException()
    {
        Should.Throw<ArgumentException>(() =>
        {
            var name = new PlayerName("");
        });
    }
}