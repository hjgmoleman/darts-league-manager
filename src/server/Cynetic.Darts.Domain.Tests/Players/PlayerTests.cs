using Cynetic.Darts.Domain.Players;
using Cynetic.Darts.Domain.Players.Events;
using Shouldly;
using Xunit;

namespace Cynetic.Darts.Domain.Tests.Players;

public class PlayerTests
{
    [Fact]
    public void Constructor_EmitsPlayerRegisteredDomainEvent()
    {
        var player = new Player(new PlayerName("T"));
        player.GetDomainEvents().ShouldContain(e => e is PlayerRegistered);
    }
}