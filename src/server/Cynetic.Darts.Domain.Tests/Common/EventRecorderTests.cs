using System.Collections;
using System.Linq;
using Cynetic.Darts.Domain.Common;
using Shouldly;
using Xunit;

namespace Cynetic.Darts.Domain.Tests.Common;

public class EventRecorderTests
{
    private record TestEvent();
    
    [Fact]
    public void Reset_ShouldClearAllEvents()
    {
        // arrange
        var recorder = new EventRecorder();
        recorder.Record(new TestEvent());
        
        // act
        recorder.Reset();
        var eventCount = recorder.Count();
        
        // assert
        eventCount.ShouldBe(0);
    }

    [Fact]
    public void GetEnumerator_ReturnsEnumerator()
    {
        var recorder = new EventRecorder();
        recorder.Record(new TestEvent());

        var enumerator = ((IEnumerable)recorder).GetEnumerator();
        
        enumerator.ShouldNotBeNull();
    }
}