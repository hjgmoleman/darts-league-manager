using Cynetic.Darts.Domain.Players;
using Cynetic.Darts.Domain.Tournaments;
using Xunit;

namespace Cynetic.Darts.Domain.Tests;

public class CarthagoDartsMasters
{
    [Fact]
    public static void Year2022()
    {
        var tournament = new Tournament(TournamentId.New, "Carthago Darts Masters 2022");

        var poolRound = new PoolTournamentRound(4);
        tournament.AddRound(poolRound);
        
        var knockoutRound = new SingleEliminationTournamentRound(SingleEliminationPlayerCount.Eight);
        tournament.AddRound(knockoutRound);
        
        var robE = new Player(new PlayerName("Rob E"));
        var simon = new Player(new PlayerName("Simon"));
        var joel = new Player(new PlayerName("Joël"));
        var hinke = new Player(new PlayerName("Hinke"));
        var pepijn = new Player(new PlayerName("Pepijn"));
        var albert = new Player(new PlayerName("Albert"));
        var vincent = new Player(new PlayerName("Vincent"));
        var bernhard = new Player(new PlayerName("Bernhard"));
        var martijn = new Player(new PlayerName("Martijn"));
        var herman = new Player(new PlayerName("Herman"));
        var guus = new Player(new PlayerName("Guus"));
        var jop = new Player(new PlayerName("Jop"));
        var robL = new Player(new PlayerName("Rob L"));
        var jordy = new Player(new PlayerName("Jordy"));
        var bjorn = new Player(new PlayerName("Björn"));
        
        poolRound.Seed(new Seed(1), robE);
        poolRound.Seed(new Seed(2), simon);
        poolRound.Seed(new Seed(3), joel);
        poolRound.Seed(new Seed(4), hinke);
        poolRound.Seed(new Seed(5), pepijn);
        poolRound.Seed(new Seed(6), albert);
        poolRound.Seed(new Seed(7), vincent);
        poolRound.Seed(new Seed(8), bernhard);
        poolRound.Seed(new Seed(9), martijn);
        poolRound.Seed(new Seed(10), herman);
        poolRound.Seed(new Seed(11), guus);
        poolRound.Seed(new Seed(12), jop);
        poolRound.Seed(new Seed(13), robL);
        poolRound.Seed(new Seed(14), jordy);
        poolRound.Seed(new Seed(15), bjorn);

        //var gameScheme = poolRound.GetScheme();
        
        knockoutRound.Seed(new Seed(1), hinke);
        knockoutRound.Seed(new Seed(2), pepijn);
        knockoutRound.Seed(new Seed(3), robE);
        knockoutRound.Seed(new Seed(4), bernhard);
        knockoutRound.Seed(new Seed(5), jop);
        knockoutRound.Seed(new Seed(6), jordy);
        knockoutRound.Seed(new Seed(7), herman);
        knockoutRound.Seed(new Seed(8), bjorn);
    }
}