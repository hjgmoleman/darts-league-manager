using System;
using Cynetic.Darts.Domain.Tournaments;
using Shouldly;
using Xunit;

namespace Cynetic.Darts.Domain.Tests.Tournaments;

public class SingleEliminationPlayerCountTests
{
    [Fact]
    public void Create_ReturnsNotNull()
    {
        var count = SingleEliminationPlayerCount.Create(2);
        count.ShouldNotBeNull();
    }
    
    [Theory]
    [InlineData(-4, 2)]
    [InlineData(1, 2)]
    [InlineData(2, 2)]
    [InlineData(3, 4)]
    [InlineData(27, 32)]
    public void Create_ReturnsMinimalContainingPlayerCount(int input, int expectedCount)
    {
        var count = SingleEliminationPlayerCount.Create(input);
        count.Count.ShouldBe(expectedCount);
    }

    [Fact]
    public void Create_GivenOverMaxCount_ThrowsArgumentOutOfRangeException()
    {
        Should.Throw<ArgumentOutOfRangeException>(() =>
        {
            SingleEliminationPlayerCount.Create(33);
        });
    }
}