using Cynetic.Darts.Domain.Players;
using Cynetic.Darts.Domain.Tournaments;
using Shouldly;
using Xunit;

namespace Cynetic.Darts.Domain.Tests.Tournaments;

public class TournamentSeedTests
{
    [Fact]
    public void Constructor_ReturnsNotNUll()
    {
        var seed = new Seed(1);
        var tournamentSeed = new TournamentSeed(seed, TournamentSeedGroup.Default);
        
        tournamentSeed.ShouldNotBeNull();
    }
    
    [Fact]
    public void Seed_ReturnsNotNull()
    {
        var seed = new Seed(1);
        var tournamentSeed = new TournamentSeed(seed, TournamentSeedGroup.Default);
        
        tournamentSeed.Seed.ShouldNotBeNull();
    }
    
    [Fact]
    public void Group_ReturnsNotNull()
    {
        var seed = new Seed(1);
        var tournamentSeed = new TournamentSeed(seed, TournamentSeedGroup.Default);
        
        tournamentSeed.Group.ShouldNotBeNull();
    }

    [Fact]
    public void AssignPlayer_SeedAlreadyTakenByAnotherPlayer_ThrowsTournamentSeedAlreadyAssignedException()
    {
        var seed = new Seed(1);
        var player1 = new Player(new PlayerName("Player 1"));
        var player2 = new Player(new PlayerName("Player 2"));
        
        var tournamentSeed = new TournamentSeed(seed, TournamentSeedGroup.Default);
        tournamentSeed.AssignPlayer(player1);
        
        // act + assert
        Should.Throw<TournamentSeedAlreadyAssignedException>(() =>
        {
            tournamentSeed.AssignPlayer(player2);
        });
    }
    
    [Fact]
    public void AssignPlayer_SeedAlreadyTakenByGivenPlayer_AssignsPlayer()
    {
        var seed = new Seed(1);
        var player1 = new Player(new PlayerName("Player 1"));
        
        var tournamentSeed = new TournamentSeed(seed, TournamentSeedGroup.Default);
        tournamentSeed.AssignPlayer(player1);
        
        // act + assert
        tournamentSeed.AssignPlayer(player1);
        
        // assert
        tournamentSeed.PlayerId.ShouldBe(player1.Id);
    }
}