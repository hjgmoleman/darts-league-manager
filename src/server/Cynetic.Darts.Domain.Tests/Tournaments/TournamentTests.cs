using System.Linq;
using Cynetic.Darts.Domain.Tournaments;
using Shouldly;
using Xunit;

namespace Cynetic.Darts.Domain.Tests.Tournaments;

public class TournamentTests
{
    [Fact]
    public void Constructor_GivenATournamentId_DoesNotEmitDomainEvents()
    {
        var id = TournamentId.New;
        var tournament = new Tournament(id, "Test");
        
        tournament.GetDomainEvents().Count().ShouldBe(0);
    }

    [Fact]
    public void Constructor_EmitsTournamentRegisteredDomainEvent()
    {
        var tournament = new Tournament("Test");
        tournament.GetDomainEvents().ShouldContain(e => e is TournamentRegistered);
    }

    [Fact]
    public void AddRound_IncrementsRoundCount()
    {
        var tournament = new Tournament("Test");
        var round = new FreeForAllTournamentRound();

        // act
        tournament.AddRound(round);
        
        // assert
        tournament.Rounds.Count.ShouldBe(1);
    }
    
    [Fact]
    public void AddRound_EmitsTournamentRoundAddedDomainEvent()
    {
        var tournament = new Tournament("Test");
        var round = new FreeForAllTournamentRound();

        // act
        tournament.AddRound(round);
        
        // assert
        tournament.GetDomainEvents().ShouldContain(e => e is TournamentRoundAdded);
    }

    [Fact]
    public void AddRound_OrdersTournamentRound()
    {
        var tournament = new Tournament(TournamentId.New, "Test");
        var round1 = new FreeForAllTournamentRound();
        var round2 = new SingleEliminationTournamentRound(SingleEliminationPlayerCount.Two);
        
        // act
        tournament.AddRound(round1);
        tournament.AddRound(round2);
        
        round2.SequenceNumber.ShouldBe(2);
    }

    [Fact]
    public void ChangeTitle_OnlyChangeIfActuallyDifferent()
    {
        // arrange
        var title = "Test";
        var tournament = new Tournament(TournamentId.New, title);
        
        // act
        tournament.ChangeTitle(title);
        
        // assert
        var events = tournament.GetDomainEvents();
        events.Count().ShouldBe(0);
    }
}