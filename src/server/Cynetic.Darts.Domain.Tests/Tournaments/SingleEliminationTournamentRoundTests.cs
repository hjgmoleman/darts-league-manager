using Cynetic.Darts.Domain.Tournaments;
using Shouldly;
using Xunit;

namespace Cynetic.Darts.Domain.Tests.Tournaments;

public class SingleEliminationTournamentRoundTests
{
    [Fact]
    public void Constructor_ReturnsNotNull()
    {
        // act
        var round = new SingleEliminationTournamentRound(SingleEliminationPlayerCount.Four);
        round.ShouldNotBeNull();
    }
    
    [Fact]
    public void PlayerCount_ReturnsConstructorValue()
    {
        var playerCount = SingleEliminationPlayerCount.Four;
        
        // act
        var round = new SingleEliminationTournamentRound(playerCount);
        round.PlayerCount.ShouldBe(playerCount);
    }
}