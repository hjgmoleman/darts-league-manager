using System;
using Cynetic.Darts.Domain.Tournaments;
using Shouldly;
using Xunit;

namespace Cynetic.Darts.Domain.Tests.Tournaments.Exceptions;

public class TournamentSeedAlreadyAssignedExceptionTests
{
    [Fact]
    public void Constructor_ReturnNotNull()
    {
        var exception = new TournamentSeedAlreadyAssignedException();
        exception.ShouldNotBeNull();
    }

    [Fact]
    public void Constructor_GivenMessage_ReturnsNotNull()
    {
        var message = "Seed is taken";
        var exception = new TournamentSeedAlreadyAssignedException(message);
        
        exception.ShouldNotBeNull();   
    }

    [Fact]
    public void Constructor_GivenMessageAndInnerException_ReturnsNotNull()
    {
        var message = "Seed is taken";
        var exception = new TournamentSeedAlreadyAssignedException(message, new Exception());
        
        exception.ShouldNotBeNull();   
    }
}