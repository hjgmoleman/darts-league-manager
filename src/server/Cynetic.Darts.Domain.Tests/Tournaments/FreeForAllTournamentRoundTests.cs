using Cynetic.Darts.Domain.Tournaments;
using Shouldly;
using Xunit;

namespace Cynetic.Darts.Domain.Tests.Tournaments;

public class FreeForAllTournamentRoundTests
{
    [Fact]
    public void Constructor_ReturnsNotNull()
    {
        // act
        var round = new FreeForAllTournamentRound();
        
        round.ShouldNotBeNull();
    }
}