using System;
using Cynetic.Darts.Domain.Tournaments;
using Shouldly;
using Xunit;

namespace Cynetic.Darts.Domain.Tests.Tournaments;

public class TournamentRoundIdTests
{
    [Fact]
    public void ImplicitCastToGuid_ReturnsNotDefault()
    {
        var guid = Guid.NewGuid();
        var id = new TournamentRoundId(guid);

        // act
        Guid asGuid = id;
        
        // assert
        asGuid.Equals(guid).ShouldBeTrue();
    }
}