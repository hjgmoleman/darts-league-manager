using System;
using Cynetic.Darts.Domain.Tournaments;
using Shouldly;
using Xunit;

namespace Cynetic.Darts.Domain.Tests.Tournaments;

public class SeedTests
{
    [Fact]
    public void Constructor_GivenArgumentLessThenOne_ThrowsArgumentOutOfRangeException()
    {
        Should.Throw<ArgumentOutOfRangeException>(() =>
        {
            var seed = new Seed(0);
        });
    }
}