using Cynetic.Darts.Domain.Players;
using Cynetic.Darts.Domain.Tournaments;
using Shouldly;
using Xunit;

namespace Cynetic.Darts.Domain.Tests.Tournaments;

public class TournamentRoundTests
{
    [Fact]
    public void Constructor_EmitsTournamentRoundRegisteredDomainEvent()
    {
        var round = new FreeForAllTournamentRound();
        round.GetDomainEvents().ShouldContain(e => e is TournamentRoundRegistered);
    }
    
    [Fact]
    public void ClearSeed_SetsPlayerIdToNull()
    {
        // arrange
        var seed = new Seed(1);
        var player = new Player(new PlayerName("Player"));
        var round = new FreeForAllTournamentRound();
        round.Seed(seed, player);
        
        // act
        round.ClearSeed(seed);
        
        // assert
        round.GetPlayer(seed).ShouldBeNull();
    }
    
    [Fact]
    public void GetPlayer_OfUnknownSeed_ReturnsNull()
    {
        var seed = new Seed(1);
        var round = new FreeForAllTournamentRound();

        var playerId = round.GetPlayer(seed);
        
        playerId.ShouldBeNull();
    }

    [Fact]
    public void SeedPlayer_EmitsTournamentSeedAssignedDomainEvent()
    {
        // arrange
        var seed = new Seed(1);
        var round = new FreeForAllTournamentRound();

        var player = new Player(new PlayerName("Player 1"));
        
        // act
        round.Seed(seed, player);
        
        // assert
        round.GetDomainEvents().ShouldContain(x => x is TournamentSeedAssigned);
    }
    
    [Fact]
    public void ClearSeed_SeedIsAssigned_EmitsTournamentSeedClearedDomainEvent()
    {
        var seed = new Seed(1);
        var round = new FreeForAllTournamentRound();
        round.Seed(seed, new Player(new PlayerName("Player")));
        round.ClearSeed(seed);
        
        round.GetDomainEvents().ShouldContain(e => e is TournamentSeedCleared);
    }
}