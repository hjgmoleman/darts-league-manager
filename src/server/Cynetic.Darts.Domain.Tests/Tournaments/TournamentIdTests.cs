using System;
using Cynetic.Darts.Domain.Tournaments;
using Shouldly;
using Xunit;

namespace Cynetic.Darts.Domain.Tests.Tournaments;

public class TournamentIdTests
{
    [Fact]
    public void ImplicitCastToGuid_ReturnsNotDefault()
    {
        var guid = Guid.NewGuid();
        var id = new TournamentId(guid);

        // act
        Guid asGuid = id;
        
        // assert
        asGuid.Equals(guid).ShouldBeTrue();
    }
}