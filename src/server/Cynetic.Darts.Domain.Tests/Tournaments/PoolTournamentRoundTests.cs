using Cynetic.Darts.Domain.Tournaments;
using Shouldly;
using Xunit;

namespace Cynetic.Darts.Domain.Tests.Tournaments;

public class PoolTournamentRoundTests
{
    [Fact]
    public void Constructor_ReturnsNotNull()
    {
        // act
        var round = new PoolTournamentRound(4);
        
        round.ShouldNotBeNull();
    }
    
    [Fact]
    public void NumberOfGroups_ReturnsConstructorValue()
    {
        var groupCount = 4;
        
        // act
        var round = new PoolTournamentRound(groupCount);
        round.NumberOfGroups.ShouldBe(groupCount);
    }
}